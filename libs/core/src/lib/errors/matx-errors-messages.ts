import { ValidationErrors } from '@angular/forms';

export type MatxErrorsMessages = {[name: string]: string | ((errors: ValidationErrors) => string)}
