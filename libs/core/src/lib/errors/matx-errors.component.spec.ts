import { waitForAsync, ComponentFixture, TestBed } from '@angular/core/testing';

import { MatxErrorsComponent } from './matx-errors.component';

describe('ErrorsComponent', () => {
  let component: MatxErrorsComponent;
  let fixture: ComponentFixture<MatxErrorsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [ MatxErrorsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MatxErrorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
