import { AfterContentInit, Component, ContentChild, ElementRef, forwardRef, Input, Renderer2 } from '@angular/core';
import { DefaultValueAccessor, FormControl, NG_VALUE_ACCESSOR, NgControl, ReactiveFormsModule } from '@angular/forms';
import { coerceBooleanProperty } from '@angular/cdk/coercion';
import { FloatLabelType, MatError, MatFormField, MatLabel } from '@angular/material/form-field';
import { MatInput } from '@angular/material/input';

@Component({
  selector: 'matx-textarea',
  templateUrl: './matx-textarea.component.html',
  styleUrls: ['./matx-textarea.component.scss'],
  imports: [MatFormField, ReactiveFormsModule, MatInput, MatError, MatLabel],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => MatxTextareaComponent),
      multi: true,
    },
  ],
})
export class MatxTextareaComponent
  extends DefaultValueAccessor
  implements AfterContentInit
{
  @Input() label?: string;

  @Input() placeholder?: string;

  @Input() required?: boolean | '' | null;

  @Input() pattern: string | RegExp = '';

  @Input() minlength: string | number | null = null;

  @Input() maxlength: string | number | null = null;

  @Input() hideRequiredMarker?: boolean | '';

  @Input() floatLabel: FloatLabelType = 'auto';

  @Input() rows?: string | number;

  @Input() set disabledControl(disabled: string | boolean | undefined) {
    this.disabled = coerceBooleanProperty(disabled);
  }

  @Input() set disabled(disabled: string | boolean) {
    if (coerceBooleanProperty(disabled)) {
      this.formControl.disable({ emitEvent: false });
    } else {
      this.formControl.enable({ emitEvent: false });
    }
  }

  @ContentChild(NgControl, { static: true }) ngControl?: NgControl;

  formControl = new FormControl();

  constructor(_renderer: Renderer2, _elementRef: ElementRef) {
    super(_renderer, _elementRef, false);
  }

  ngAfterContentInit() {
    this.formControl.valueChanges.subscribe((value) => this.onChange(value));
    if (this.ngControl && this.ngControl.statusChanges) {
      this.ngControl.control?.statusChanges.subscribe(() => {
        this.formControl.setErrors(this.ngControl?.errors ?? {});
      });
    }
  }

  override writeValue(value: unknown): void {
    this.formControl.setValue(value, { emitEvent: false });
  }
}
