import { waitForAsync, ComponentFixture, TestBed } from '@angular/core/testing';

import { MatxDatepickerComponent } from './matx-datepicker.component';
import { provideNativeDateAdapter } from '@angular/material/core';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

describe('MatxDatepickerComponent', () => {
  let component: MatxDatepickerComponent;
  let fixture: ComponentFixture<MatxDatepickerComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [MatxDatepickerComponent, NoopAnimationsModule],
      providers: [provideNativeDateAdapter()],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MatxDatepickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
