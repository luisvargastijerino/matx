import { Component, HostBinding, Input, OnInit } from '@angular/core';
import { MatxNavTreeItem } from './matx-nav-tree-item';
import { NavigationEnd, Router, RouterLink } from '@angular/router';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { first } from 'rxjs';
import { MatListItem, MatListItemIcon, MatListItemTitle, MatNavList } from '@angular/material/list';
import { NgClass, NgStyle } from '@angular/common';
import { MatIcon } from '@angular/material/icon';
import { MatIconButton } from '@angular/material/button';

@Component({
  selector: 'matx-nav-tree',
  templateUrl: './matx-nav-tree.component.html',
  styleUrl: './matx-nav-tree.component.scss',
  imports: [
    MatNavList,
    NgStyle,
    MatListItem,
    MatListItemTitle,
    MatListItemIcon,
    NgClass,
    MatIcon,
    RouterLink,
    MatIconButton,
  ],
  animations: [
    trigger('indicatorRotate', [
      state('collapsed', style({ transform: 'rotate(-90deg)' })),
      state('expanded', style({ transform: 'rotate(0deg)' })),
      transition(
        'expanded <=> collapsed',
        animate('225ms cubic-bezier(0.4,0.0,0.2,1)')
      ),
    ]),
  ],
})
export class MatxNavTreeComponent implements OnInit {
  @HostBinding('attr.aria-expanded') expanded: boolean[] = [];
  @Input() items?: MatxNavTreeItem[];
  @Input() depth = 0;

  constructor(public router: Router) {}

  ngOnInit(): void {
    if (this.router.url === '/') {
      this.router.events
        .pipe(first((e) => e instanceof NavigationEnd))
        .subscribe(() => this.updateExpanded());
    } else {
      this.updateExpanded();
    }
  }

  private updateExpanded() {
    this.expanded =
      this.items?.map(
        (item) => !!item.children?.length && this.isItemRouteActive(item)
      ) ?? [];
  }

  isItemRouteActive(item: MatxNavTreeItem) {
    return (
      !!item.route &&
      this.router.isActive(item.route, {
        paths: 'subset',
        queryParams: 'ignored',
        fragment: 'ignored',
        matrixParams: 'ignored',
      })
    );
  }

  toggleExpand(index: number) {
    this.expanded[index] = !this.expanded[index];
  }
}
