export interface MatxNavTreeItem {
  displayName: string;
  disabled?: boolean;
  iconName?: string;
  route?: string;
  href?: string;
  children?: MatxNavTreeItem[];
  onItemSelected?: (item: MatxNavTreeItem, index: number) => boolean | void;
}
