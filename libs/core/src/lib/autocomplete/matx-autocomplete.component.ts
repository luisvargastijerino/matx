import {
  AfterViewInit,
  Component,
  ContentChild,
  ElementRef,
  forwardRef,
  Input,
  OnDestroy,
  OnInit,
  Renderer2,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import {
  DefaultValueAccessor,
  FormsModule,
  NG_VALUE_ACCESSOR,
  NgControl,
  NgModel,
} from '@angular/forms';
import { BehaviorSubject, Observable, of, Subscription } from 'rxjs';
import {
  MatAutocomplete,
  MatAutocompleteSelectedEvent,
  MatAutocompleteTrigger,
  MatOption,
} from '@angular/material/autocomplete';
import { debounceTime, skip, switchMap, tap } from 'rxjs/operators';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { MatxAutocompleteTemplateDirective } from './matx-autocomplete-template.directive';
import { coerceBooleanProperty } from '@angular/cdk/coercion';
import { MatxOption } from '../matx-option';
import {
  FloatLabelType,
  MatError,
  MatFormField,
  MatLabel,
} from '@angular/material/form-field';
import { MatChipGrid, MatChipInput, MatChipRow } from '@angular/material/chips';
import { MatIcon } from '@angular/material/icon';
import { MatProgressSpinner } from '@angular/material/progress-spinner';
import { NgStyle, NgTemplateOutlet } from '@angular/common';
import { MatIconButton } from '@angular/material/button';

@Component({
  selector: 'matx-autocomplete',
  templateUrl: './matx-autocomplete.component.html',
  styleUrls: ['./matx-autocomplete.component.scss'],
  imports: [
    MatFormField,
    MatChipGrid,
    MatChipRow,
    MatIcon,
    MatAutocompleteTrigger,
    MatChipInput,
    MatAutocomplete,
    MatOption,
    MatProgressSpinner,
    NgStyle,
    NgTemplateOutlet,
    FormsModule,
    MatIconButton,
    MatError,
    MatLabel,
  ],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => MatxAutocompleteComponent),
      multi: true,
    },
  ],
})
export class MatxAutocompleteComponent
  extends DefaultValueAccessor
  implements OnInit, AfterViewInit, OnDestroy
{
  filteredOptions: MatxOption[] = [];

  readonly separatorKeysCodes = [ENTER, COMMA];

  private _required = false;

  get required(): boolean {
    return this._required;
  }

  @Input() set required(value: boolean | '') {
    this._required = coerceBooleanProperty(value);
  }

  private _multiple = false;

  get multiple(): boolean {
    return this._multiple;
  }

  @Input() set multiple(value: boolean | '' | undefined) {
    this._multiple = coerceBooleanProperty(value);
  }

  private _repeatable = false;

  get repeatable(): boolean {
    return this._repeatable;
  }

  @Input() set repeatable(value) {
    this._repeatable = coerceBooleanProperty(value);
  }

  @Input() placeholder?: string;

  @Input() label?: string;

  @Input() displayField?: string;

  @Input() filterBy?: (filterVal: string) => Observable<MatxOption[]>;

  @Input() options: MatxOption[] | Observable<MatxOption[]> = [];

  private _hideRequiredMarker = false;

  get hideRequiredMarker(): boolean {
    return this._hideRequiredMarker;
  }

  @Input() set hideRequiredMarker(value: boolean | '') {
    this._hideRequiredMarker = coerceBooleanProperty(value);
  }

  @Input() floatLabel: FloatLabelType = 'auto';

  @Input() autocomplete = 'off';

  private _filterChange$ = new BehaviorSubject<string>('');

  private _disabled = false;

  get disabled(): boolean {
    return this._disabled;
  }

  @Input() set disabled(disabled: boolean) {
    this._disabled = coerceBooleanProperty(disabled);
  }

  @Input() set disabledControl(disabled: boolean) {
    this.disabled = disabled;
  }

  private _displayWith?: (option: MatxOption) => string;

  get displayWith(): (option: MatxOption) => string {
    return (option) =>
      (!option
        ? ''
        : typeof option === 'string' || typeof option === 'number'
        ? option
        : this._displayWith
        ? this._displayWith(option)
        : option[this.displayField ?? 'name']) as string;
  }

  @Input() set displayWith(
    _displayWith: ((option: MatxOption) => string) | undefined
  ) {
    this._displayWith = _displayWith;
  }

  loading = false;

  _selectedValue?:
    | string
    | Record<string, string>
    | (string | Record<string, string>)[];

  _selectedOptions: (string | Record<string, string>)[] = [];

  private subscription?: Subscription;

  @ViewChild('inputEl') inputEl!: ElementRef<HTMLInputElement>;

  @ViewChild('autocompleteTrigger', { static: true })
  autocompleteTrigger?: MatAutocompleteTrigger;

  @ViewChild('chipGridEl') chipGrid?: MatChipGrid;

  @ViewChild('chipListModel') chipListModel?: NgModel;

  @ContentChild(NgControl, { static: true }) ngControl?: NgControl;

  @Input() set template(template: TemplateRef<unknown> | undefined | null) {
    this._template = template ?? null;
  }

  @ContentChild(MatxAutocompleteTemplateDirective, {
    read: TemplateRef,
    static: true,
  })
  _template: TemplateRef<unknown> | null = null;

  @Input() optionStyle: { [klass: string]: unknown } | null = null;

  @Input() chipStyle: { [klass: string]: unknown } | null = null;

  constructor(_renderer: Renderer2, _elementRef: ElementRef<HTMLElement>) {
    super(_renderer, _elementRef, false);
  }

  ngOnInit() {
    // console.log('this.options: ', this.options)
    if (!(this.options instanceof Observable)) {
      this.subscription = this._filterChange$
        .pipe(skip(1))
        .subscribe((value) => {
          // console.log('value: ', value)
          this.filteredOptions = (this.options as MatxOption[]).filter(
            (option) =>
              this.displayWith(option)
                .toLowerCase()
                .includes(value.toLowerCase())
          );
          this.autocompleteTrigger?.openPanel();
        });
    } else {
      this.subscription = this._filterChange$
        .pipe(
          skip(1),
          tap(() => (this.loading = true)),
          debounceTime(500),
          // tap(value => console.log('value: ', value)),
          switchMap((value) => this.filterBy?.(value) ?? of([]))
        )
        .subscribe((values) => {
          this.loading = false;
          this.filteredOptions = values;
          this.autocompleteTrigger?.openPanel();
        });
    }
  }

  ngAfterViewInit(): void {
    if (this.ngControl && this.ngControl.statusChanges) {
      this.ngControl.control?.statusChanges.subscribe(() => {
        this.chipListModel?.control?.setErrors(this.ngControl?.errors ?? {});
      });
      setTimeout(() => {
        this.chipListModel?.control?.setErrors(this.ngControl?.errors ?? {});
      });
    }
  }

  ngOnDestroy(): void {
    this.subscription?.unsubscribe();
  }

  _filterBy() {
    this._filterChange$.next(this.inputEl.nativeElement.value);
  }

  search(event: MouseEvent) {
    event.stopPropagation();
    this._filterBy();
  }

  clear(event: MouseEvent) {
    event.stopPropagation();
    if (this.multiple && !this.inputEl.nativeElement.value) {
      this._selectedOptions = [];
    }
    this.inputEl.nativeElement.value = '';
    this._filterChange$.next('');
    this.onChange(null);
  }

  override writeValue(
    value: string | Record<string, string> | (string | Record<string, string>)[]
  ) {
    if (this.multiple) {
      this._selectedValue = this._selectedOptions = value as (
        | string
        | Record<string, string>
      )[];
    } else {
      this._selectedValue = value;
      if (value) {
        this._selectedOptions.push(value as string | Record<string, string>);
      }
    }
  }

  selectOption(event: MatAutocompleteSelectedEvent) {
    if (this._multiple) {
      if (
        this.repeatable ||
        !this._selectedOptions.some(
          (o) => event.option.viewValue === this.displayWith(o)
        )
      ) {
        this._selectedOptions.push(event.option.value);
      }
    } else {
      this._selectedValue = event.option.value;
      this._selectedOptions[0] = this._selectedValue as
        | string
        | Record<string, string>;
    }
    this.inputEl.nativeElement.value = '';
    setTimeout(() => this.chipGrid?.writeValue(this._selectedValue));
    // this.chipList._onChange(this._selectedValue);
    this.onChange(this._selectedValue);
  }

  removeSelected(selectedIndex: number) {
    this._selectedOptions.splice(selectedIndex, 1);
    if (!this.multiple) {
      delete this._selectedValue;
    }
    this._filterChange$.next('');
    this.chipGrid?._onChange(this._selectedValue);
    this.onChange(this._selectedValue);
    setTimeout(() => this.inputEl.nativeElement.focus());
  }

  editSelected(selectedIndex: number) {
    if (this.disabled) {
      return;
    }
    this.inputEl.nativeElement.value = this.displayWith(
      this._selectedOptions[selectedIndex]
    );
    this.removeSelected(selectedIndex);
    this._filterBy();
    setTimeout(() => this.inputEl.nativeElement.focus());
  }
}
