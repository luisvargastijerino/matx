import { Directive } from '@angular/core';

@Directive({ selector: '[matxAutocompleteTemplate]' })
export class MatxAutocompleteTemplateDirective {}
