import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogContent, MatDialogRef, MatDialogTitle } from '@angular/material/dialog';
import { LoadingData } from './loading.models';
import { MatProgressSpinner } from '@angular/material/progress-spinner';

@Component({
  selector: 'matx-loading',
  templateUrl: './matx-loading.component.html',
  imports: [MatDialogTitle, MatDialogContent, MatProgressSpinner],
  styleUrls: ['./matx-loading.component.scss'],
})
export class MatxLoadingComponent {
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: LoadingData,
    private dialogRef: MatDialogRef<unknown>
  ) {}
}
