import { Injectable } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { delay } from 'rxjs';

@Injectable({providedIn: 'root'})
export class MatxSidenavMenuService {

  constructor(private breakpointObserver: BreakpointObserver) {
    this.breakpointObserver.observe([
      Breakpoints.XSmall,
      Breakpoints.Small,
    ]).subscribe(result => {
      this.isMobile = result.matches;
      this.opened = !this.isMobile;
    });
  }

  opened = false;

  isMobile = false;

}
