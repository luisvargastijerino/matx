import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatxMenuButtonComponent } from './matx-menu-button.component';

describe('MenuButtonComponent', () => {
  let component: MatxMenuButtonComponent;
  let fixture: ComponentFixture<MatxMenuButtonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [MatxMenuButtonComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(MatxMenuButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
