import { Component } from '@angular/core';
import { MatxSidenavMenuService } from './matx-sidenav-menu.service';
import { MatIcon } from '@angular/material/icon';
import { MatIconButton } from '@angular/material/button';

@Component({
  selector: 'matx-menu-button',
  imports: [MatIcon, MatIconButton],
  templateUrl: './matx-menu-button.component.html',
})
export class MatxMenuButtonComponent {
  constructor(public sidenavMenuCtrl: MatxSidenavMenuService) {}
}
