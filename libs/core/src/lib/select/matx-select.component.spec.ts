import { waitForAsync, ComponentFixture, TestBed } from '@angular/core/testing';

import { MatxSelectComponent } from './matx-select.component';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

describe('MatxSelectComponent', () => {
  let component: MatxSelectComponent;
  let fixture: ComponentFixture<MatxSelectComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [ MatxSelectComponent, NoopAnimationsModule ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MatxSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
