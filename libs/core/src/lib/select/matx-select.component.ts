import {
  AfterViewInit,
  Component,
  ContentChild,
  ElementRef,
  forwardRef,
  Input,
  OnDestroy,
  OnInit,
  Renderer2,
} from '@angular/core';
import {
  DefaultValueAccessor,
  FormControl,
  NG_VALUE_ACCESSOR,
  NgControl,
  ReactiveFormsModule,
} from '@angular/forms';
import { BehaviorSubject, Observable, Subscription } from 'rxjs';
import { coerceBooleanProperty } from '@angular/cdk/coercion';
import { MatxOption } from '../matx-option';
import {
  FloatLabelType,
  MatError,
  MatFormField, MatLabel
} from '@angular/material/form-field';
import { MatOption, MatSelect } from '@angular/material/select';
import { AsyncPipe } from '@angular/common';

@Component({
  selector: 'matx-select',
  templateUrl: './matx-select.component.html',
  styleUrls: ['./matx-select.component.scss'],
  imports: [
    MatFormField,
    MatSelect,
    ReactiveFormsModule,
    MatOption,
    AsyncPipe,
    MatError,
    MatLabel,
  ],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => MatxSelectComponent),
      multi: true,
    },
  ],
})
export class MatxSelectComponent
  extends DefaultValueAccessor
  implements OnInit, AfterViewInit, OnDestroy
{
  @Input() set disabledControl(disabled: string | boolean) {
    this.disabled = disabled;
  }

  @Input() set disabled(disabled: string | boolean) {
    if (coerceBooleanProperty(disabled)) {
      this.formControl.disable({ emitEvent: false });
    } else {
      this.formControl.enable({ emitEvent: false });
    }
  }

  _displayWith?: (option: MatxOption) => string;

  get displayWith(): (option: MatxOption) => string {
    return (option) =>
      (!option
        ? ''
        : this._displayWith
        ? this._displayWith(option)
        : typeof option === 'string' || typeof option === 'number'
        ? option
        : option[this.displayField ?? '']) as string;
  }

  @Input()
  set displayWith(displayWith: ((option: MatxOption) => string) | undefined) {
    this._displayWith = displayWith;
  }

  constructor(_renderer: Renderer2, _elementRef: ElementRef) {
    super(_renderer, _elementRef, false);
  }

  @Input() options?:
    | MatxOption[]
    | Observable<MatxOption[]>
    | BehaviorSubject<MatxOption[]>;

  options$ = new BehaviorSubject<MatxOption[]>([]);

  @Input() label?: string;

  @Input() placeholder?: string;

  private _required = false;

  get required(): boolean {
    return this._required;
  }

  @Input() set required(value: boolean | '') {
    this._required = coerceBooleanProperty(value);
  }

  @Input() displayField?: string;

  @Input() compareField?: string;

  @Input() valueField?: string;

  @Input() indexValue?: boolean | '' | null;

  @Input() noneText?: string | null;

  private _hideRequiredMarker = false;

  get hideRequiredMarker(): boolean {
    return this._hideRequiredMarker;
  }

  @Input() set hideRequiredMarker(value: boolean | '') {
    this._hideRequiredMarker = coerceBooleanProperty(value);
  }

  @Input() floatLabel: FloatLabelType = 'auto';

  @Input() compareWith?: (option: any, value: any) => boolean;

  private _multiple = false;

  get multiple(): boolean {
    return this._multiple;
  }

  @Input() set multiple(value: boolean | '' | undefined) {
    this._multiple = coerceBooleanProperty(value);
  }

  @ContentChild(NgControl, { static: true }) ngControl?: NgControl;

  formControl = new FormControl();

  private subscription?: Subscription;

  _compareWith = (option: MatxOption, value: MatxOption): boolean => {
    return (
      option !== null &&
      option !== undefined &&
      value !== null &&
      value !== undefined &&
      (option === value ||
        (this.compareWith?.(option, value) ??
          (typeof option === 'object' &&
            ((!!this.compareField &&
              (typeof value === 'string' || typeof value === 'number'
                ? option[this.compareField] === value
                : option[this.compareField] === value[this.compareField])) ||
              (!!this.displayField &&
                option[this.displayField] ===
                  (value as Record<string, unknown>)[this.displayField])))))
    );
  };

  ngOnInit() {
    this.subscription = this.formControl.valueChanges.subscribe((value) =>
      this.onChange(value)
    );
    if (this.options instanceof BehaviorSubject) {
      this.options$ = this.options;
    } else if (this.options instanceof Observable) {
      this.subscription.add(
        this.options.subscribe((options) => this.options$.next(options))
      );
    } else {
      this.options$.next(this.options ?? []);
    }
  }

  _getValue = (i: number, option: MatxOption) => {
    return this.indexValue === '' || this.indexValue === true
      ? i
      : this.valueField && typeof option === 'object'
      ? option[this.valueField]
      : option;
  };

  ngAfterViewInit(): void {
    if (this.ngControl && this.ngControl.statusChanges) {
      this.subscription?.add(
        this.ngControl.control?.statusChanges.subscribe(() =>
          this.formControl.setErrors(this.ngControl?.errors ?? {})
        )
      );
      setTimeout(() =>
        this.ngControl?.control?.setValue(this.formControl.value, {
          emitEvent: false,
        })
      );
    }
  }

  ngOnDestroy(): void {
    this.subscription?.unsubscribe();
  }

  override writeValue(value: unknown): void {
    this.formControl.setValue(
      !this._multiple ||
        value === null ||
        value === undefined ||
        Array.isArray(value)
        ? value
        : [value]
    );
  }
}
