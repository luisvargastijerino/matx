import {
  AfterContentInit,
  Component,
  ContentChild,
  ElementRef,
  forwardRef,
  Input,
  Renderer2,
} from '@angular/core';
import {
  DefaultValueAccessor,
  FormControl,
  NG_VALUE_ACCESSOR,
  NgControl,
  ReactiveFormsModule,
} from '@angular/forms';
import { coerceBooleanProperty } from '@angular/cdk/coercion';
import {
  FloatLabelType,
  MatError,
  MatFormField,
  MatLabel,
  MatPrefix,
  MatSuffix,
} from '@angular/material/form-field';
import { MatInput } from '@angular/material/input';

@Component({
  selector:
    'matx-input, matx-input[ngModel], matx-input[formControl], matx-input[formControlName], matx-input[ngDefaultControl]',
  templateUrl: './matx-input.component.html',
  styleUrls: ['./matx-input.component.css'],
  imports: [
    MatFormField,
    ReactiveFormsModule,
    MatInput,
    MatError,
    MatLabel,
    MatPrefix,
    MatSuffix,
  ],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => MatxInputComponent),
      multi: true,
    },
  ],
})
export class MatxInputComponent
  extends DefaultValueAccessor
  implements AfterContentInit
{
  @Input() label = '';

  @Input() placeholder = '';

  private _required = false;
  get required(): boolean {
    return this._required;
  }

  @Input() set required(value: boolean | '') {
    this._required = coerceBooleanProperty(value);
  }

  @Input() pattern = '';

  @Input() type:
    | 'text'
    | 'password'
    | 'tel'
    | 'number'
    | 'email'
    | 'search'
    | 'url' = 'text';

  @Input() min: string | number | null = null;

  @Input() max: string | number | null = null;

  @Input() minlength: string | number | null = null;

  @Input() maxlength: string | number | null = null;

  @Input() step: string | number | null = null;

  private _hideRequiredMarker = false;
  get hideRequiredMarker(): boolean {
    return this._hideRequiredMarker;
  }

  @Input() set hideRequiredMarker(value: boolean | '') {
    this._hideRequiredMarker = coerceBooleanProperty(value);
  }

  @Input() floatLabel: FloatLabelType = 'auto';

  @Input() autocomplete = 'off';

  @Input() set disabledControl(disabled: string | boolean) {
    this.disabled = disabled;
  }

  @Input() set disabled(disabled: string | boolean) {
    if (coerceBooleanProperty(disabled)) {
      this.formControl.disable({ emitEvent: false });
    } else {
      this.formControl.enable({ emitEvent: false });
    }
  }

  @ContentChild(NgControl, { static: true }) ngControl?: NgControl;

  formControl = new FormControl();

  constructor(_renderer: Renderer2, _elementRef: ElementRef) {
    super(_renderer, _elementRef, false);
  }

  ngAfterContentInit() {
    this.formControl.valueChanges.subscribe((value) => {
      if (this.type === 'number') {
        this.onChange(Number(value));
      } else {
        this.onChange(value);
      }
    });
    if (this.ngControl && this.ngControl.statusChanges) {
      this.ngControl.control?.statusChanges.subscribe(() => {
        this.formControl.setErrors(this.ngControl?.errors ?? {});
      });
    }
  }

  override writeValue(value: unknown): void {
    this.formControl.setValue(value, { emitEvent: false });
  }
}
