import { Component, Inject, OnInit } from '@angular/core';
import {
  MAT_DIALOG_DATA,
  MatDialogActions,
  MatDialogContent,
  MatDialogRef,
  MatDialogTitle
} from '@angular/material/dialog';
import { MatxPromptAction, MatxPromptData } from './matx-prompt.models';
import { FormControl, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { MatxInputComponent } from '../input/matx-input.component';
import { MatxErrorsComponent } from '../errors/matx-errors.component';
import { MatxSelectComponent } from '../select/matx-select.component';
import { MatxAutocompleteComponent } from '../autocomplete/matx-autocomplete.component';
import { MatxDatepickerComponent } from '../datepicker/matx-datepicker.component';
import { MatxTextareaComponent } from '../textarea/matx-textarea.component';
import { MatButton } from '@angular/material/button';
import { MatProgressSpinner } from '@angular/material/progress-spinner';


@Component({
  selector: 'matx-prompt',
  templateUrl: './matx-prompt.component.html',
  imports: [
    MatDialogTitle,
    ReactiveFormsModule,
    MatDialogContent,
    MatxInputComponent,
    MatxErrorsComponent,
    MatxSelectComponent,
    MatxAutocompleteComponent,
    MatxDatepickerComponent,
    MatxTextareaComponent,
    MatDialogActions,
    MatButton,
    MatProgressSpinner,
  ],
  styleUrls: ['./matx-prompt.component.scss'],
})
export class MatxPromptComponent implements OnInit {
  form = new FormGroup({});

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: MatxPromptData,
    private dialogRef: MatDialogRef<unknown>
  ) {}

  ngOnInit() {
    for (const input of this.data.inputs || []) {
      this.form.addControl(
        input.name,
        new FormControl(input.value, input.validators)
      );
    }
  }

  getActionText(action: MatxPromptAction | string) {
    return typeof action === 'string' ? action : action.text;
  }

  async execActionCb(action: MatxPromptAction) {
    if (action.callback) {
      try {
        if (this.form.invalid) {
          return;
        }
        if (action.showLoading) {
          action._loading = true;
        }
        await action.callback(this.form.value);
        this.dialogRef.close();
        if (action.showLoading) {
          action._loading = false;
        }
      } catch (e) {
        if (action.showLoading) {
          action._loading = false;
        }
        throw e;
      }
    } else {
      this.dialogRef.close();
    }
  }

  withForm(value: unknown, form: FormGroup) {
    return typeof value === 'function' ? value(form) : value;
  }
}
