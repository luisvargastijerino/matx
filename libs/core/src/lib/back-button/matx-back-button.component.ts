import { Component } from '@angular/core';
import { Location } from '@angular/common';
import { MatIcon } from '@angular/material/icon';
import { MatIconButton } from '@angular/material/button';

@Component({
  selector: 'matx-back-button',
  templateUrl: './matx-back-button.component.html',
  imports: [MatIcon, MatIconButton],
})
export class MatxBackButtonComponent {
  constructor(public location: Location) {}
}
