# matx-core

This project contains some useful components to extends [Angular Material](https://material.angular.io/)


[demo](https://luisvargastijerino.gitlab.io/matx/core)

# Components

- [matx-input](https://luisvargastijerino.gitlab.io/matx/core/#/matx-input)
- [matx-autocomplete](https://luisvargastijerino.gitlab.io/matx/core/#/matx-autocomplete)
- [matx-datepicker](https://luisvargastijerino.gitlab.io/matx/core/#/matx-datepicker)
- [matx-gmap-autocomplete](https://luisvargastijerino.gitlab.io/matx/core/#/matx-gmap-autocomplete)
- [matx-back-button](https://luisvargastijerino.gitlab.io/matx/core/#/matx-back-button)
- [matx-menu-button](https://luisvargastijerino.gitlab.io/matx/core/#/matx-menu-button)
- [matx-select](https://luisvargastijerino.gitlab.io/matx/core/#/matx-select)
- [matx-prompt](https://luisvargastijerino.gitlab.io/matx/core/#/matx-prompt)
- [matx-nav-tree](https://luisvargastijerino.gitlab.io/matx/core/#/matx-nav-tree)

## Building

Run `nx build core`

## Running unit tests

Run `nx test core` to execute the unit tests.
