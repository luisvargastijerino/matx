/*
Arrays Query TS - A lightweight query API for Typescript collections
(c)2022 - Luis Vargas
May be freely distributed according to MIT license.

This is small library that provides a query api for JavaScript arrays similar to *mongo db*.
The aim of the project is to provide a simple, well tested, way of filtering data in JavaScript.
 */

function indexOf(this: (string | RegExp)[], item: string) {
  for (let i = 0; i < this.length; i++) {
    const element = this[i];
    if (element instanceof RegExp) {
      if (element.test(item)) return i;
    } else {
      if (element === item) return i;
    }
  }
  return -1;
}

/* UTILS */
const utils = {
  result: (obj: any, key: any) => {
    if (obj == null) {
      obj = {};
    }
    if (utils.getType(obj[key]) === "Function") {
      return obj[key]();
    } else {
      return obj[key];
    }
  },
  /*detect: (array, fn) => {
    let i, item, len;
    for (i = 0, len = array.length; i < len; i++) {
      item = array[i];
      if (fn(item)) {
        return item;
      }
    }
  },
  reject: (array, fn) => {
    let i, item, len;
    const results: unknown[] = [];
    for (i = 0, len = array.length; i < len; i++) {
      item = array[i];
      if (!fn(item)) {
        results.push(item);
      }
    }
    return results;
  },*/
  intersection: <T>(array1: T[], array2: T[]) => {
    let i, item, len;
    const results: T[] = [];
    for (i = 0, len = array1.length; i < len; i++) {
      item = array1[i];
      if (array2.indexOf(item) !== -1) {
        results.push(item);
      }
    }
    return results;
  },
  isEqual: (a: any, b: any) => JSON.stringify(a) === JSON.stringify(b),
  getType(obj: any) {
    const type = Object.prototype.toString.call(obj).substring(8);
    return type.substring(0, type.length - 1);
  },

  makeObj(key: string, val: any) {
    return {[key]: val};
  },

  reverseString(str: string) {
    return str.toLowerCase().split('').reverse().join('');
  },

  compoundKeys: ["$and", "$not", "$or", "$nor"],

  makeGetter(keys: string) {
    const _keys = keys.split(".");
    return (obj: Record<string, any>) => {
      let i, key, len, out;
      out = obj;
      for (i = 0, len = _keys.length; i < len; i++) {
        key = _keys[i];
        if (out) {
          out = utils.result(out, key);
        }
      }
      return out;
    };
  }
}

function multipleConditions(key: string, queries: Record<string, any>) {
  let type, val;
  const results: unknown[] = [];
  for (type in queries) {
    val = queries[type];
    results.push(utils.makeObj(key, utils.makeObj(type, val)));
  }
  return results;
}

interface ParamType {
  key: string | null;
  boost: unknown;
  getter: (key: Record<string, any>) => any;
  type: string;
  value: RegExp | Date | object | ParamType[] | ((model: any) => boolean) | string | boolean | null | number
}

function parseParamType(query: any) {
  const key = Object.keys(query)[0];
  const queryParam = query[key];
  let o = {
    key
  } as ParamType;
  if (queryParam != null ? queryParam.$boost : void 0) {
    o.boost = queryParam.$boost;
    delete queryParam.$boost;
  }
  if (key.indexOf(".") !== -1) {
    o.getter = utils.makeGetter(key);
  }
  const paramType = utils.getType(queryParam);
  switch (paramType) {
    case "RegExp":
    case "Date":
      o.type = `$${paramType.toLowerCase()}`;
      o.value = queryParam;
      break;
    case "Object":
      if (indexOf.call(utils.compoundKeys, key) >= 0) {
        o.type = key;
        o.value = parseSubQuery(queryParam);
        o.key = null;
      } else if (Object.keys(queryParam).length > 1) {
        o.type = "$and";
        o.value = parseSubQuery(multipleConditions(key, queryParam));
        o.key = null;
      } else {
        for (const type in queryParam) {
          if (!queryParam.hasOwnProperty(type)) continue;
          const value = queryParam[type];
          if (testQueryValue(type as QueryType, value)) {
            o.type = type;
            switch (type) {
              case "$elemMatch":
                o.value = single(parseQuery(value));
                break;
              case "$endsWith":
                o.value = utils.reverseString(value);
                break;
              case "$likeI":
              case "$startsWith":
                o.value = value.toLowerCase();
                break;
              case "$not":
              case "$nor":
              case "$or":
              case "$and":
                o.value = parseSubQuery(utils.makeObj(o.key ?? '', value));
                o.key = null;
                break;
              case "$computed":
                o = parseParamType(utils.makeObj(key, value));
                o.getter = utils.makeGetter(key);
                break;
              default:
                o.value = value;
            }
          } else {
            throw new Error("Query value (" + value + ") doesn't match query type: (" + type + ")");
          }
        }
      }
      break;
    default:
      o.type = "$equal";
      o.value = queryParam;
  }
  if ((o.type === "$equal") && (paramType === "Object" || paramType === "Array")) {
    o.type = "$deepEqual";
  }
  return o;
}

function parseSubQuery(rawQuery: any) {
  let i, key, len, query, queryArray, val;
  if (Array.isArray(rawQuery)) {
    queryArray = rawQuery;
  } else {
    queryArray = (function () {
      const results: unknown[] = [];
      for (key in rawQuery) {
        if (!rawQuery.hasOwnProperty(key)) continue;
        val = rawQuery[key];
        results.push(utils.makeObj(key, val));
      }
      return results;
    })();
  }
  const results: ParamType[] = [];
  for (i = 0, len = queryArray.length; i < len; i++) {
    query = queryArray[i];
    results.push(parseParamType(query));
  }
  return results;
}

export type QueryType = '$in' | '$nin' | '$all' | '$any' | '$size' | '$regex' | '$regexp' | '$like' | '$likeI' | '$type'
  | '$equal' | '$deepEqual' | '$ne' | '$lt' | '$gt' | '$lte' | '$gte' | '$betweene' | '$between' | '$exists' | '$has'
  | '$mod' | '$cb' | '$startsWith' | '$endsWith' | '$contains' | '$elemMatch' | '$and' | '$or' | '$not' | '$nor';

function testQueryValue(queryType: QueryType, value: any) {
  const valueType = utils.getType(value);
  switch (queryType) {
    case "$in":
    case "$nin":
    case "$all":
    case "$any":
      return valueType === "Array";
    case "$size":
      return valueType === "Number";
    case "$regex":
    case "$regexp":
      return valueType === "RegExp";
    case "$like":
    case "$likeI":
      return valueType === "String";
    case "$between":
    case "$mod":
      return (valueType === "Array") && (value.length === 2);
    case "$cb":
      return valueType === "Function";
    default:
      return true;
  }
}

function testModelAttribute(queryType: QueryType, value: any) {
  const valueType = utils.getType(value);
  switch (queryType) {
    case "$like":
    case "$likeI":
    case "$regex":
    case "$startsWith":
    case "$endsWith":
      return valueType === "String";
    case "$contains":
    case "$all":
    case "$any":
    case "$elemMatch":
      return valueType === "Array";
    case "$size":
      return valueType === "String" || valueType === "Array";
    case "$in":
    case "$nin":
      return value != null;
    default:
      return true;
  }
}

function performQuery(type: QueryType, value: any, attr: any, model: any) {
  switch (type) {
    case "$equal":
      if (Array.isArray(attr)) {
        return indexOf.call(attr, value) >= 0;
      } else {
        return attr === value;
      }
    case "$deepEqual":
      return utils.isEqual(attr, value);
    case "$contains":
      return indexOf.call(attr, value) >= 0;
    case "$ne":
      return attr !== value;
    case "$lt":
      return attr < value;
    case "$gt":
      return attr > value;
    case "$lte":
      return attr <= value;
    case "$gte":
      return attr >= value;
    case "$between":
      return (value[0] < attr && attr < value[1]);
    case "$betweene":
      return (value[0] <= attr && attr <= value[1]);
    case "$in":
      return indexOf.call(value, attr) >= 0;
    case "$nin":
      return indexOf.call(value, attr) < 0;
    case "$all":
      return value.every((item: any) => indexOf.call(attr, item) >= 0);
    case "$any":
      return attr.some((item: any) => indexOf.call(value, item) >= 0);
    case "$size":
      return attr.length === value;
    case "$exists":
    case "$has":
      return (attr != null) === value;
    case "$like":
      return attr.indexOf(value) !== -1;
    case "$likeI":
      return attr.toLowerCase().indexOf(value) !== -1;
    case "$startsWith":
      return attr.toLowerCase().indexOf(value) === 0;
    case "$endsWith":
      return utils.reverseString(attr).indexOf(value) === 0;
    case "$type":
      return typeof attr === value;
    case "$regex":
    case "$regexp":
      return value.test(attr);
    case "$cb":
      return value.call(model, attr);
    case "$mod":
      return (attr % value[0]) === value[1];
    case "$elemMatch":
      return attr.find(where(value));
    case "$and":
    case "$or":
    case "$nor":
    case "$not":
      return performQuerySingle(type, value, model);
    default:
      return false;
  }
}

type QueryObject = {
  type: QueryType;
  parsedQuery: any;
}

function single(queries: any[], getter?: Getter<any>, isScore = false) {
  let queryObj: QueryObject;
  if (isScore) {
    if (queries.length !== 1) {
      throw new Error("score operations currently don't work on compound queries");
    }
    queryObj = queries[0];
    if (queryObj.type !== "$and") {
      throw new Error("score operations only work on $and queries (not " + queryObj.type);
    }
    return function (model: any) {
      model._score = performQuerySingle(queryObj.type, queryObj.parsedQuery, model, true);
      return model;
    };
  } else {
    return function (model: any) {
      for (const item of queries) {
        queryObj = item;
        if (!performQuerySingle(queryObj.type, queryObj.parsedQuery, model, isScore)) {
          return false;
        }
      }
      return true;
    };
  }
}

function performQuerySingle(type: QueryType, query: any, model?: any, isScore = false) {
  let attr, boost, q, ref, test, passes = 0, score = 0;
  const scoreInc = 1 / query.length;
  for (let i = 0; i < query.length; i++) {
    q = query[i];
    if (q.getter) {
      attr = q.getter(model, q.key);
    } else {
      attr = model[q.key];
    }
    test = testModelAttribute(q.type, attr);
    if (test) {
      test = performQuery(q.type, q.value, attr, model);
    }
    if (test) {
      passes++;
      if (isScore) {
        boost = (ref = q.boost) != null ? ref : 1;
        score += scoreInc * boost;
      }
    }
    switch (type) {
      case "$and":
        if (!isScore) {
          if (!test) {
            return false;
          }
        }
        break;
      case "$not":
        if (test) {
          return false;
        }
        break;
      case "$or":
        if (test) {
          return true;
        }
        break;
      case "$nor":
        if (test) {
          return false;
        }
        break;
      default:
        throw new Error("Invalid compound method");
    }
  }
  if (isScore) {
    return score;
  } else if (type === "$not") {
    return passes === 0;
  } else {
    return type !== "$or";
  }
}

interface ParsedQuery {
  type: QueryType;
  parsedQuery: ParamType[]
}

function parseQuery(query: any): ParsedQuery[] {
  let key, type, val;
  const queryKeys = Object.keys(query);
  if (!queryKeys.length) {
    return [];
  }
  const compoundQuery = utils.intersection(utils.compoundKeys, queryKeys);
  if (compoundQuery.length === 0) {
    return [{
      type: "$and",
      parsedQuery: parseSubQuery(query)
    }];
  } else {
    if (compoundQuery.length !== queryKeys.length) {
      if (indexOf.call(compoundQuery, "$and") < 0) {
        query.$and = {};
        compoundQuery.unshift("$and");
      }
      for (key in query) {
        if (!query.hasOwnProperty(key)) continue;
        val = query[key];
        if (!(indexOf.call(utils.compoundKeys, key) < 0)) {
          continue;
        }
        query.$and[key] = val;
        delete query[key];
      }
    }
    let i, len;
    const results: ParsedQuery[] = [];
    for (i = 0, len = compoundQuery.length; i < len; i++) {
      type = compoundQuery[i] as QueryType;
      results.push({
        type,
        parsedQuery: parseSubQuery(query[type])
      });
    }
    return results;
  }
}

type GetterFn<T> = (obj: T, key: keyof T) => T[keyof T];
type Getter<T> = keyof T | string | GetterFn<T>;

class QueryBuilder<T> {
  private readonly theQuery: Query<T>;
  private indexes: unknown;

  constructor(
    private items?: T[],
    private _getter?: Getter<T>) {
    this.theQuery = {} as Query<T>;
  }

  all(items?: T[]) {
    if (items) {
      this.items = items;
    }
    if (this.indexes) {
      items = this.getIndexedItems(this.items);
    } else {
      items = this.items ?? [];
    }
    return items.filter(where(this.theQuery)) as (T | Scored<T>)[];
  }

  /*tester() {
    return makeTest(this.theQuery, this._getter);
  }*/

  first<T>(items: T[]) {
    return items.find(where(this.theQuery));
  }

  getter(_getter: Getter<T>) {
    this._getter = _getter;
    return this;
  }

  run = this.all

  private getIndexedItems(items?: T[]) {
    return items ?? []
  }

  private addToQuery(type: string) {
    return (params: any, qVal?: any) => {
      let base: Record<string, any>;
      if (qVal) {
        params = utils.makeObj(params, qVal);
      }
      if ((base = (this.theQuery as Record<string, any>))[type] == null) {
        base[type] = [];
      }
      (this.theQuery as Record<string, any>)[type].push(params);
      return this;
    };
  }

  and = this.addToQuery('$and')
  not = this.addToQuery('$not')
  or = this.addToQuery('$or')
  nor = this.addToQuery('$nor')
}

export function query<T>(items?: T[], getter?: Getter<T>) {
  return new QueryBuilder(items, getter);
}

type Scored<T> = T & { _score: number };

export type QStringMatcher = {
  $cb?: (attr?: string) => boolean,
  $like?: string,
  $likeI?: string,
  $startsWith?: string,
  $endsWith?: string,
  $contains?: string;
  $in?: string | string[];
  $ne?: string;
  $equal?: string;
  $regex?: RegExp;
  $not?: string | QStringMatcher;
};

export type QNumberMatcher = {
  $cb?: (attr?: number) => boolean,
  $gt?: number,
  $gte?: number,
  $lt?: number,
  $lte?: number,
  $between?: [number, number],
  $betweene?: [number, number],
  $mod?: [number, number],
  $ne?: number;
  $equal?: number;
  $in?: number | number[];
  $not?: number | QNumberMatcher;
};

export type QDateMatcher = {
  $cb?: (attr?: Date) => boolean,
  $gt?: Date,
  $gte?: Date,
  $lt?: Date,
  $lte?: Date,
  $between?: [Date, Date],
  $betweene?: [Date, Date],
  $ne?: Date;
  $equal?: Date;
  $in?: Date | Date[];
  $not?: Date | QDateMatcher;
};

export type QArrayMatcher = {
  $cb?: (attr?: unknown[]) => boolean,
  $contains?: unknown[][number],
  $any?: unknown[],
  $size?: number,
  $elemMatch?: unknown,
}

export type QUnknownMatcher = {
  $cb?: (attr?: unknown) => boolean,
  $ne?: unknown
  $equal?: unknown,
  $contains?: unknown,
  $in?: unknown | unknown[],
  $nin?: unknown | unknown[],
  $all?: unknown | unknown[],
  $any?: unknown | unknown[],
  $size?: number,
  $exists?: boolean,
  $regex?: RegExp,
  $not?: QUnknownMatcher,
  $elemMatch?: QUnknownMatcher,
  $boost?: number
}

export type QMatcher<T> =
  T extends string ? string | RegExp | QStringMatcher
    : T extends number ? number | QNumberMatcher
      : T extends Date ? Date | QDateMatcher
        : T extends Array<unknown> ? QArrayMatcher
          : QUnknownMatcher;

export type SimpleQuery<T> = {
  [P in keyof T]: QMatcher<T[P]>;
}

export type Query<T> = {
  [P in keyof T]: T[P] | QMatcher<T[P]>;
} | {
  $and?: Query<T> | Query<T>[];
  $or?: Query<T> | Query<T>[];
  $not?: Query<T>;
  $nor?: Query<T> | Query<T>[];
} | Record<string, string | number | RegExp | Date | QUnknownMatcher> /*| ((T) => boolean)*/;

export function where<I, T extends Partial<I>>(query: Query<T>): ((item: I) => boolean) {
  if (typeof query !== "function") {
    const queries = parseQuery(query);
    return model => {
      for (const queryObj of queries) {
        if (!performQuerySingle(queryObj.type, queryObj.parsedQuery, model, false)) {
          return false;
        }
      }
      return true;
    };
  }
  return query
}

export function score<I, T extends Partial<I>>(items: I[], query: Query<T>) {
  return items.map(single(parseQuery(query), undefined, true) as ((model: any) => Scored<I>));
}
