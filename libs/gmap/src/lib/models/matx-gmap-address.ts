
export interface MatxGmapAddress {
  showSuggestions?: boolean;
  address?: string;
  coordinates?: {latitude?: number, longitude?: number};
  place_id?: string;
}
