import {
  Component,
  ElementRef,
  forwardRef,
  Input,
  NgZone,
  OnInit,
  Renderer2,
} from '@angular/core';
import {
  DefaultValueAccessor,
  FormControl,
  NG_VALUE_ACCESSOR,
  ReactiveFormsModule,
} from '@angular/forms';
import { MapsAPILoader } from '@ng-maps/core';
import { debounceTime } from 'rxjs/operators';
import { EMPTY } from 'rxjs';
import { MatxGmapAddress } from '../models/matx-gmap-address';
import { MatDialog } from '@angular/material/dialog';
import { MatxGmapSearchDialogComponent } from '../gmap-search-dialog/matx-gmap-search-dialog.component';
import { coerceBooleanProperty } from '@angular/cdk/coercion';
import { FloatLabelType, MatError, MatFormField, MatLabel, MatSuffix } from '@angular/material/form-field';
import {
  MatAutocomplete,
  MatAutocompleteTrigger,
  MatOption,
} from '@angular/material/autocomplete';
import { MatIcon } from '@angular/material/icon';
import { MatProgressSpinner } from '@angular/material/progress-spinner';
import Geocoder = google.maps.Geocoder;
import AutocompleteService = google.maps.places.AutocompleteService;
import { MatIconButton } from '@angular/material/button';
import { MatInput } from '@angular/material/input';

@Component({
  selector:
    'matx-gmap-autocomplete,' +
    ' matx-gmap-autocomplete[ngModel],' +
    ' matx-gmap-autocomplete[name],' +
    ' matx-gmap-autocomplete[formControl],' +
    ' matx-gmap-autocomplete[formControlName]' +
    ' matx-gmap-autocomplete[ngDefaultControl]',
  templateUrl: './matx-gmap-autocomplete.component.html',
  styleUrls: ['./matx-gmap-autocomplete.component.scss'],
  imports: [
    MatFormField,
    MatAutocompleteTrigger,
    ReactiveFormsModule,
    MatIcon,
    MatProgressSpinner,
    MatAutocomplete,
    MatOption,
    MatError,
    MatLabel,
    MatIconButton,
    MatInput,
    MatSuffix,
  ],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => MatxGmapAutocompleteComponent),
      multi: true,
    },
  ],
})
export class MatxGmapAutocompleteComponent
  extends DefaultValueAccessor
  implements OnInit
{
  autocompleteService!: AutocompleteService;

  geocoder!: Geocoder;
  options?: MatxGmapAddress[] = [];
  formControl = new FormControl<MatxGmapAddress | string | null>(null);
  loading = false;

  @Input() label = '';
  @Input() placeholder = '';

  private _required: boolean | '' = '';
  get required(): boolean {
    return coerceBooleanProperty(this._required);
  }
  @Input() set required(value: boolean | '') {
    this._required = value;
  }

  private _hideRequiredMarker: boolean | '' = '';
  get hideRequiredMarker(): boolean {
    return coerceBooleanProperty(this._hideRequiredMarker);
  }
  @Input() set hideRequiredMarker(value: boolean | '') {
    this._hideRequiredMarker = value;
  }

  @Input() floatLabel: FloatLabelType = 'auto';

  @Input() set disabledControl(disabled: string | boolean) {
    this.disabled = disabled;
  }

  @Input() set disabled(disabled: string | boolean) {
    if (disabled === '' || disabled === true) {
      this.formControl.disable({ emitEvent: false });
    } else {
      this.formControl.enable({ emitEvent: false });
    }
  }

  constructor(
    private mapsApiLoader: MapsAPILoader,
    private matDialog: MatDialog,
    private ngZone: NgZone,
    _renderer: Renderer2,
    _elementRef: ElementRef
  ) {
    super(_renderer, _elementRef, false);
  }

  ngOnInit() {
    this.formControl.valueChanges.pipe(debounceTime(500)).subscribe((value) => {
      if (!value) {
        this.onChange(value);
        return EMPTY;
      }
      if (
        typeof value === 'object' &&
        !!Object.getOwnPropertyDescriptor(value, 'place_id')
      ) {
        this.geocoder.geocode({ placeId: value.place_id }, (results) => {
          this.ngZone.run(() => {
            this.formControl.setValue(<MatxGmapAddress>{
              address: results?.[0].formatted_address,
              coordinates: {
                latitude: results?.[0].geometry.location.lat(),
                longitude: results?.[0].geometry.location.lng(),
              },
            });
          });
        });
      }

      if (Object.getOwnPropertyDescriptor(value, 'coordinates')) {
        this.onChange(value);
      }

      if (typeof value === 'string') {
        this.onChange(undefined);
        this.formControl.setErrors({ required: { value: undefined } });
      }

      if (typeof value !== 'string') {
        return EMPTY;
      }

      this.autocompleteService.getQueryPredictions(
        { input: value },
        (autocompletePredictions) =>
          this.ngZone.run(() => {
            if (autocompletePredictions) {
              this.options = autocompletePredictions.map((result) => ({
                address: result.description,
                place_id: result.place_id,
              }));
            } else {
              this.geocoder.geocode({ address: value }, (geocodeResults) =>
                this.ngZone.run(() => {
                  this.options = geocodeResults?.map((result) => ({
                    address:
                      result.formatted_address ||
                      `${result.geometry.location.lat()}, ${result.geometry.location.lng()}`,
                    coordinates: {
                      latitude: result.geometry.location.lat(),
                      longitude: result.geometry.location.lng(),
                    },
                  }));
                })
              );
            }
          })
      );

      return EMPTY;
    });

    this.mapsApiLoader.load().then(() => {
      this.autocompleteService = new google.maps.places.AutocompleteService();
      this.geocoder = new google.maps.Geocoder();
    });
  }

  override writeValue(address: MatxGmapAddress): void {
    this.formControl.setValue(address);
  }

  displaySelectedAddress(option: { address: string }) {
    return option?.address ?? '';
  }

  getMyCurrentAddress(event: MouseEvent) {
    event.stopPropagation();
    let address: MatxGmapAddress | null;
    if (typeof this.formControl.value === 'string') {
      address = { address: this.formControl.value };
    } else {
      address = this.formControl.value;
    }
    if (address && address.coordinates) {
      this.matDialog
        .open(MatxGmapSearchDialogComponent, {
          maxHeight: '650px',
          maxWidth: '800px',
          width: '90%',
          height: '90%',
          data: address,
        })
        .afterClosed()
        .subscribe((result) => {
          if (result) {
            this.formControl.setValue(result);
          }
        });
    } else if ('geolocation' in navigator) {
      this.loading = true;
      navigator.geolocation.getCurrentPosition((result) => {
        this.loading = false;
        this.matDialog
          .open(MatxGmapSearchDialogComponent, {
            maxHeight: '650px',
            maxWidth: '800px',
            width: '90%',
            height: '90%',
            data: <MatxGmapAddress>{
              showSuggestions: !!(address && address.address),
              address:
                (address && address.address) ||
                `${result.coords.latitude}, ${result.coords.longitude}`,
              coordinates: {
                latitude: result.coords.latitude,
                longitude: result.coords.longitude,
              },
            },
          })
          .afterClosed()
          .subscribe((result2) => {
            if (result2) {
              this.formControl.setValue(result2);
            }
          });
      });
    }
  }
}
