/// <reference types="@types/google.maps" />
export * from './lib/gmap-autocomplete/matx-gmap-autocomplete.component';
export * from './lib/gmap-search-dialog/matx-gmap-search-dialog.component';
export * from './lib/models/matx-gmap-address';
