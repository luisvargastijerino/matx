import { Directive } from '@angular/core';

@Directive({selector: 'ng-template[dtPrintable]'})
export class MatxPrintableDirective {}
