import { Directive } from '@angular/core';

@Directive({
  selector: 'ng-template[matxExpandRowButton]'
})
export class MatxExpandRowButtonDirective {}
