import { Directive } from '@angular/core';

@Directive({selector: 'ng-template[matxEditor]'})
export class MatxEditorDirective {}
