import { ContentChild, Directive, Input, TemplateRef, OnInit } from '@angular/core';
import { MatxColumn } from './matx-column';
import { MatxCellDirective } from './matx-cell.directive';
import { NgClass, NgStyle } from '@angular/common';
import { coerceBooleanProperty } from '@angular/cdk/coercion';
import { QueryType } from 'array-query-ts';

@Directive({
  selector: 'ng-template[matxColumn]',
  providers: [
    { provide: MatxColumn, useExisting: MatxColumnTemplateDirective },
  ],
})
export class MatxColumnTemplateDirective extends MatxColumn implements OnInit {

  /** Handles if the column will be frozen at left or right */
  @Input() frozen: 'left' | 'right' | '' = '';

  /** Column field. It could be either direct property name or dot-property name */
  @Input() field = '';

  /** Column Header string */
  @Input() header = '';

  /** Column ngClass */
  @Input() ngClass: NgClass['ngClass'] = {};

  /** Column ngStyle */
  @Input() columnNgStyle: { [klass: string]: any } = {};

  /** Style used when column is exported to Excel format */
  @Input() xlsColumnStyle: NgStyle['ngStyle'];

  /** Column width */
  @Input() width?: string;

  /** Column minimum width */
  @Input() minWidth = '150px';

  /** Width used when column is exported to Excel */
  @Input() xlsWidth?: number;

  @Input() tooltip = '';

  private _sortBy: string | ((row: unknown) => unknown) = '';

  /**
   * Field name or function used to sort the column
   * <br>
   * The value could be a direct property:
   *  ```html
   *  <matx-column sortBy="name"></matx-column>
   *  ```
   *
   *  Or it could be a dot property:
   *  ```html
   *  <matx-column sortBy="user.name"></matx-column>
   *  ```
   *
   *  Or it could be a function:
   *  ```html
   *  <matx-column [sortBy]="sortByUserName"></matx-column>
   *  ```
   *  ```ts
   *  sortByUsername = (row) => row.user.name;
   *  ```
   *
   */
  get sortBy(): string | ((row: any) => any) {
    return this.field || this._sortBy;
  }

  @Input() set sortBy(value: string | ((row: any) => any)) {
    this._sortBy = value;
  }

  private _sortable = false;

  /** Determines if the column is sortable. If [sortBy] is present this attribute is ignored. */
  get sortable(): boolean {
    return this._sortable || !!this._sortBy;
  }

  @Input() set sortable(value: boolean | '') {
    this._sortable = coerceBooleanProperty(value);
  }

  sortDirection: 1 | 0 | -1 = 0;

  private _noPrintable = false;

  /** Determines if the column is not printable */
  get noPrintable(): boolean {
    return this._noPrintable;
  }

  @Input() set noPrintable(value: boolean | '') {
    this._noPrintable = coerceBooleanProperty(value);
  }

  private _noResizable = false;

  /** Determines if the column is not printable */
  get noResizable(): boolean {
    return this._noResizable;
  }

  @Input() set noResizable(value: boolean | '') {
    this._noResizable = coerceBooleanProperty(value);
  }

  @Input() comparison: QueryType = '$likeI';

  @Input() comparisonFn!: (item: unknown, filter: unknown) => boolean;

  ngOnInit(): void {
    if (
      this.width &&
      this.width.includes('px') &&
      Number(this.width.replace('px', '')) < 150
    ) {
      this.minWidth = this.width;
    }
  }
}
