import _get from 'lodash-es/get';

export function compare(props: (string | ((el: unknown) => unknown))[], directions: number[]): (a: unknown, b: unknown) => number {
  return (a, b) => {
    let ret = 0;

    props.some((el, i) => {
      let x;
      let y;

      if (typeof el === 'function') {
        x = el(a);
        y = el(b);
      } else {
        x = _get(a, el);
        y = _get(b, el);
      }

      if (x === y) {
        ret = 0;
        return false;
      }

      if (typeof x === 'string' && typeof y === 'string') {
        ret = x.localeCompare(y) * directions[i];
        return ret !== 0;
      }

      ret = (x < y ? -1 : 1) * directions[i];
      return true;
    });

    return ret;
  };
}
