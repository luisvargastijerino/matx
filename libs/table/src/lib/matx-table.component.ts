import {
  AfterContentChecked,
  Component,
  ContentChild,
  ContentChildren,
  ElementRef,
  EventEmitter,
  HostBinding,
  Input,
  OnDestroy,
  OnInit,
  Output,
  TemplateRef,
  ViewChild,
  ViewChildren,
} from '@angular/core';
import {
  debounceTime,
  delay,
  firstValueFrom,
  Observable,
  of,
  Subscription,
} from 'rxjs';
import { MatxColumnComponent } from './matx-column.component';
import { MatxRowDetailDirective } from './matx-row-detail.directive';
import { FormControl, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { coerceBooleanProperty } from '@angular/cdk/coercion';
import { compare } from './compare';
import { MatxRow } from './matx-row';
import {
  CdkFixedSizeVirtualScroll,
  CdkVirtualForOf,
  CdkVirtualScrollViewport,
} from '@angular/cdk/scrolling';
import { MatxGroupHeaderDirective } from './matx-group-header.directive';
import { NgClass, NgStyle, NgTemplateOutlet } from '@angular/common';
import { MatxCustomFiltersDirective } from './matx-custom-filters.directive';
import {
  CdkDrag,
  CdkDragDrop,
  CdkDropList,
  moveItemInArray,
} from '@angular/cdk/drag-drop';
import _throttle from 'lodash-es/throttle';
import _debounce from 'lodash-es/debounce';
import _get from 'lodash-es/get';
import _set from 'lodash-es/set';
import { Clipboard } from '@angular/cdk/clipboard';
import { Query, where } from 'array-query-ts';
import { MatIcon } from '@angular/material/icon';
import { MatCheckbox } from '@angular/material/checkbox';
import { MatxInputComponent } from 'matx-core';
import { MatProgressBar } from '@angular/material/progress-bar';
import { MatxColumnResizableDirective } from './matx-column-resizable-directive';
import { MatxColumnResizeGrabberDirective } from './matx-column-resize-grabber.directive';
import { MatIconButton } from '@angular/material/button';
import { MatTooltip } from '@angular/material/tooltip';
import { MatxColumn } from './matx-column';
import { MatSuffix } from '@angular/material/form-field';

export type MatxControl =
  | 'search-box'
  | 'columns-filter-button'
  | 'print-button'
  | 'copy-csv-button'
  | 'new-button'
  | 'view-more-button'
  | 'delete-button'
  | 'refresh-button';

@Component({
  selector: 'matx-table',
  templateUrl: './matx-table.component.html',
  imports: [
    NgClass,
    NgTemplateOutlet,
    MatIcon,
    NgStyle,
    MatCheckbox,
    MatxInputComponent,
    ReactiveFormsModule,
    MatProgressBar,
    CdkFixedSizeVirtualScroll,
    CdkVirtualScrollViewport,
    CdkDropList,
    CdkDrag,
    MatxColumnResizableDirective,
    MatxColumnResizeGrabberDirective,
    CdkVirtualForOf,
    MatIconButton,
    MatTooltip,
    MatSuffix,
  ],
  styleUrls: ['./matx-table.component.scss'],
})
export class MatxTableComponent
  implements OnInit, AfterContentChecked, OnDestroy
{
  private dataChangeSubs!: Subscription;

  private searchBoxChangeSubs!: Subscription;

  private columnsFiltersChangeSubs!: Subscription;

  _viewportScrollbarWidth!: number;

  @ViewChild('viewport', { read: ElementRef })
  viewport!: ElementRef<HTMLElement>;
  @ViewChild('virtualScroller') virtualScroller!: CdkVirtualScrollViewport;

  @Input() id = '';

  @Input() controls: string /*| MatxControl | MatxControl[]*/ = '';

  @Input() xlsTitle = '';

  @Input() exportableFileName = '';

  /** Function used to get value from object using `dot.property` notation */
  get: (object: unknown, path: string) => unknown = _get;

  /** Sets the page index to be rendered (only for local/sync data) */
  @Input() pageIndex = 0;

  /** Sets the page size to be rendered (only for local/sync data) */
  @Input() pageSize = 20;

  @Output() totalItems = new EventEmitter<number>();

  /** Sets the data and maps it to respective row values */
  @Input() set data(items: unknown[] | Observable<unknown[]>) {
    // console.log('items: ', items);
    if (this._selectedRows) {
      this._selectedRows.clear();
    }

    if (items instanceof Observable) {
      this.dataChangeSubs?.unsubscribe();
      this.dataChangeSubs = items.subscribe(this.mapRows);
    } else {
      this.mapRows(items);
      this.clonedRows = [...this.rows];
      if (!this.remote) this._sortRows();
      setTimeout(() => {
        if (this.selectedItems) {
          this.selectRowsFromSelectedItems();
        }
        if (this.selectedRowIndex && this.selectedRowIndex < this.rows.length) {
          this.toggleSelectRow(this.rows[this.selectedRowIndex], true);
        }
      }, 100);
      this.totalItems.emit(this.clonedRows.length);
    }
  }

  private _editable = false;

  /** Handles if the table is editable by row */
  get editable(): boolean {
    return this._editable;
  }

  @Input() set editable(value: boolean | '') {
    this._editable = coerceBooleanProperty(value);
  }

  @ViewChildren('renderedColumn') renderedColumns!: MatxColumn;

  private _selectable: '' | 'single' | 'multiple' = '';

  /** Handles if the table is selectable and shows the select column */
  get selectable(): '' | 'single' | 'multiple' {
    return this._selectable;
  }

  @Input() set selectable(value: '' | 'single' | 'multiple') {
    this._selectable = value === '' ? 'multiple' : value;
  }

  /** Handles the style of the scrollable columns area */
  @Input() scrollableStyle: NgStyle['ngStyle'];

  /** Handles the style of the frozen left columns area */
  frozenLeftStyle: NgStyle['ngStyle'];

  /** Handles the style of the scrollable columns area */
  frozenRightStyle: NgStyle['ngStyle'];

  /** Url used to fetch print styles */
  @Input() printStyleUrl = '';

  /** Print Css Styles */
  @Input() printStyleCss =
    '.matx-cell, .matx-header-cell {' +
    'border-bottom: 3px solid rgba(0, 0, 0, 0.12);' +
    'padding-left: 10px;padding-right: 10px;' +
    'text-align: left;' +
    '}' +
    '.matx-cell {' +
    'border-bottom-width: 1px' +
    '}' +
    'input[type=checkbox] { display: none; }';

  /** Event emitted after a column sort is started. This event is only used for remote/async sorting. */
  @Output() sort = new EventEmitter<string>();

  @Output() refreshClicked = new EventEmitter<void>();

  private sortedColumnsSet = new Set<MatxColumnComponent>();

  private sortedColumns: MatxColumnComponent[] = [];

  /** Event emitted after a column sort is started. This event is only used for remote/async sorting. */
  @Output() columnsQueryChange = new EventEmitter<Query<never>>();

  /** Event emitted after a column sort is started. This event is only used for remote/async sorting. */
  @Output() searchTextChange = new EventEmitter<string>();

  @Output() viewMoreClicked = new EventEmitter<void>();

  private _remote = false;

  /** Defines if it will emit sort or filter event instead running them locally */
  get remote(): boolean {
    return this._remote;
  }

  @Input() set remote(value: boolean | '') {
    this._remote = coerceBooleanProperty(value);
  }

  /** Sets the style of the row depending on the item value of the row */
  @Input() rowStyle?: (item: any) => { [style: string]: any };

  @Output() allRowsSelected = new EventEmitter<boolean>();

  @Output() rowClick = new EventEmitter<MatxRow>();

  @HostBinding('style.height') get _dtTableHeight() {
    return Math.min(this.rows.length * 50 + 150, 500) + 'px';
  }

  private getColumnWidth = (c: MatxColumnComponent): string =>
    c.columnNgStyle['width'] ??
    c.width ??
    c.columnNgStyle['min-width'] ??
    c.minWidth;

  private get frozenLeftWidths(): string[] {
    const widths = this.frozenLeftColumns.map((c) => this.getColumnWidth(c));
    if (this.rowDetail) {
      widths.push('50px');
    }
    if (this._selectable) {
      widths.push('60px');
    }
    if (this.reorderableRows) {
      widths.push('45px');
    }
    return widths;
  }

  private get frozenRightWidths(): string[] {
    return this.frozenRightColumns.map((c) => this.getColumnWidth(c));
  }

  private get scrollableWidths(): string[] {
    const widths = this.scrollableColumns.map((c) => this.getColumnWidth(c));
    widths.push(...this.frozenLeftWidths, ...this.frozenRightWidths);
    return widths;
  }

  private _setColumnsWidths = _throttle(() => {
    this.frozenLeftColumns.length = 0;
    this.frozenRightColumns.length = 0;
    this.scrollableColumns.length = 0;
    this.origScrollableColumns.length = 0;
    this.columns.forEach((column) => {
      if (column.tooltip == '') {
        column.tooltip = column.header;
      }
      if (column.frozen === 'left') {
        this.frozenLeftColumns.push(column);
      } else if (column.frozen === 'right') {
        this.frozenRightColumns.push(column);
      } else {
        this.origScrollableColumns.push(column);
      }

      if (column.width) {
        column.columnNgStyle['flex'] = '0 1 ' + column.width;
      }
    });
    if (this._sortedScrollableColumns) {
      this.scrollableColumns = this._sortedScrollableColumns.map(
        (v) => this.origScrollableColumns[Number(v)]
      );
    } else {
      this.scrollableColumns = this.origScrollableColumns;
    }

    this.frozenLeftStyle = {
      ...this.frozenLeftStyle,
      width: `calc(${this.frozenLeftWidths.join(' + ')})`,
    };
    this.frozenRightStyle = {
      ...this.frozenRightStyle,
      width: `calc(${this.frozenRightWidths.join(' + ')})`,
    };
    this.scrollableStyle = {
      ...this.scrollableStyle,
      'min-width': `calc(${this.scrollableWidths.join(' + ')})` /*,
      width: `calc(${this.scrollableWidths.join(' + ')})`*/,
    };

    this._viewportScrollbarWidth = this.viewport?.nativeElement
      ? this.viewport.nativeElement.offsetWidth -
        this.viewport.nativeElement.clientWidth
      : 15;
  }, 200);

  /** List of columns added in the content */
  @ContentChildren(MatxColumn) columns!: MatxColumnComponent[];

  /** List of frozen left columns */
  frozenLeftColumns: MatxColumnComponent[] = [];

  /** List of scrollable columns */
  scrollableColumns: MatxColumnComponent[] = [];

  private origScrollableColumns: MatxColumnComponent[] = [];

  private _sortedScrollableColumns?: string[];

  /** List of frozen right columns */
  frozenRightColumns: MatxColumnComponent[] = [];

  /** List of rows */
  rows: MatxRow[] = [];

  private clonedRows: MatxRow[] = [];

  private filteredRows!: MatxRow[];

  /** Set of selected rows */
  _selectedRows = new Set<MatxRow>();

  @Input() selectedRowIndex = 0;

  @Input() selectedItems: never[] = [];

  @Input() selectedItemsComparisonKey = 'id';

  /** Handles if all rows are selected */
  get allSelected(): boolean {
    return this.rows.length === this._selectedRows.size;
  }

  /** Handles if some rows are selected */
  get someSelected(): boolean {
    return this._selectedRows.size > 0 && !this.allSelected;
  }

  /** Template of the detail row content */
  @ContentChild(MatxRowDetailDirective, { read: TemplateRef, static: true })
  rowDetail!: TemplateRef<never>;

  /** Element used to handle printable table */
  @ViewChild('printable') _printable!: ElementRef;

  /** Element used to handle printable table */
  @ViewChild('excelTable') _excelTable!: ElementRef;

  /** Handles if the printable table should be rendered */
  renderPrintable = false;

  /** Handles if the exportable table should be rendered */
  renderExportable = false;

  renderExcel = false;
  /** Shows/Hides the loading element */
  @Input() loading = false;

  /** Handles the groupBy field or function */
  @Input() groupBy: string | ((item: any) => any) = '';

  /** Template used to create GroupBy Header */
  @ContentChild(MatxGroupHeaderDirective, { read: TemplateRef })
  groupHeaderTemplate!: TemplateRef<never>;

  @ContentChild(MatxCustomFiltersDirective, { read: TemplateRef })
  customFiltersTemplate!: TemplateRef<never>;

  /** Filter to be applied */
  private _columnsQuery: Query<never> = {};

  searchBoxControl = new FormControl();

  /** Emits add new Element when the new button is clicked */
  @Output() addNew = new EventEmitter<void>();

  /** Emits delete selected elements Element when the new button is clicked */
  @Output() deleteSelected = new EventEmitter<any[]>();

  @Output() rowsSelected = new EventEmitter<any[]>();

  @Output() rowReset = new EventEmitter<MatxRow>();

  /** Handles if the rows are reorderable by dragging and dropping */
  @Input() reorderableRows?: 'internal' | 'external';

  /** Emits when rows are dropped */
  @Output() rowDropped = new EventEmitter<CdkDragDrop<MatxRow[], MatxRow>>();

  /** Applies the filter to the rows if the data is local, else it emits the filter event containing the filters to be applied */
  applyFilter = _debounce((_columnsQuery: Query<any>) => {
    if (this.dataChangeSubs || this.remote) {
      this.columnsQueryChange.emit(_columnsQuery);
      return;
    }
    this._columnsQuery = _columnsQuery;
    this.filterRows();
  }, 500);

  /** Filters form row */
  columnsFiltersForm!: FormGroup;

  /** Handles if the filter row is shown */
  showColumnsFilters = false;

  constructor(private clipboard: Clipboard) {}

  ngOnInit(): void {
    this.searchBoxControl = new FormControl();
    const searchTextChanges$ = this.searchBoxControl.valueChanges.pipe(
      debounceTime(500)
    );
    if (this.dataChangeSubs || this.remote) {
      this.searchBoxChangeSubs = searchTextChanges$.subscribe((value) =>
        this.searchTextChange.emit(value)
      );
    } else {
      this.searchBoxChangeSubs = searchTextChanges$.subscribe(() =>
        this.filterRows()
      );
    }
  }

  ngAfterContentChecked(): void {
    this._setColumnsWidths();
  }

  ngOnDestroy(): void {
    this.dataChangeSubs?.unsubscribe();
    this.searchBoxChangeSubs?.unsubscribe();
    this.columnsFiltersChangeSubs?.unsubscribe();
  }

  private mapRows = (items: any[]) => {
    if (this.groupBy) {
      const groups = items.reduce((prev, item) => {
        if (typeof this.groupBy === 'string') {
          const key = this.groupBy;
          (prev[_get(item, key) as string] =
            prev[_get(item, key) as string] || []).push(item);
        } else {
          const key = this.groupBy(item);
          (prev[key] = prev[key] || []).push(item);
        }
        return prev;
      }, {});
      this.rows = Object.keys(groups).reduce((prev: any[], key) => {
        const parent = {
          item: key,
          creating: false,
          editing: false,
          expanded: true,
        } as MatxRow;
        const children = groups[key].map(
          (item: any) =>
            ({
              item,
              editing: false,
              creating: false,
              expanded: false,
              parent,
            } as MatxRow)
        );
        parent.children = children;
        prev.push(parent, ...children);
        return prev;
      }, []);
    } else {
      this.rows =
        items?.map(
          (item, index) =>
            ({
              item,
              editing: false,
              creating: false,
              expanded: false,
              index,
            } as MatxRow)
        ) ?? [];
    }
  };

  /** Paginates rows. This should only run if data is local. */
  paginate(rows: MatxRow[]): MatxRow[] {
    return !this._remote &&
      this.pageIndex &&
      this.pageIndex > 0 &&
      this.pageSize &&
      this.pageSize > 0
      ? rows.slice(
          (this.pageIndex - 1) * this.pageSize,
          this.pageIndex * this.pageSize
        )
      : rows;
  }

  /** Selects/Unselects all rows */
  toggleSelectAll(event: any): void {
    if (event === true || event.checked || event.target?.checked) {
      this.allRowsSelected.emit(true);
      this.rows.forEach((row) => this._selectedRows.add(row));
    } else {
      this.allRowsSelected.emit(false);
      this._selectedRows.clear();
    }
    this.rowsSelected.emit(this.selectedRows);
  }

  /** Selects/Unselects specified row */
  toggleSelectRow(row: MatxRow, event: any): void {
    if (this._selectable === 'single') {
      this._selectedRows.clear();
    }

    if (event === true || event.checked || event.target?.checked) {
      this._selectedRows.add(row);
    } else {
      this._selectedRows.delete(row);
    }
    this.rowsSelected.emit(this.selectedRows);
  }

  /** Returns an array of selected rows */
  get selectedRows() {
    return Array.from(this._selectedRows).map((r) => r.item);
  }

  private selectRowsFromSelectedItems() {
    this.rows
      ?.filter((r) =>
        this.selectedItems.some((sr) =>
          this.selectedItemsComparisonKey
            ? this.get(sr, this.selectedItemsComparisonKey) ===
              this.get(r.item, this.selectedItemsComparisonKey)
            : sr === r.item
        )
      )
      .forEach((r) => this._selectedRows.add(r));
  }

  /** Handles if row is selected */
  isRowSelected(row: MatxRow): boolean {
    return this._selectedRows.has(row);
  }

  /** Handles the start of adding a new row */
  startAdding(item: any = {}) {
    const row = { item, creating: true, index: 0 } as MatxRow;
    this.rows = [row, ...this.rows.map((r) => ({ ...r, index: r.index + 1 }))];
    this.startEditing(row);
  }

  /** Handles the start of editing specified row. For example when user double-clicks the row */
  startEditing(row: MatxRow): void {
    if (!this.editable || row.editing) {
      return;
    }
    row.form = new FormGroup({});
    this.columns.forEach((c) =>
      row.form?.addControl(c.field, new FormControl(_get(row.item, c.field)))
    );
    row.editing = true;
  }

  /** Handles the reset of specified row. For example when users press key Esc */
  resetRow(row: MatxRow): void {
    this.columns.forEach((c) => row.form?.removeControl(c.field));
    delete row.form;
    row.editing = false;

    if (row.creating) {
      this.rows = this.rows.slice(1).map((r) => ({ ...r, index: r.index - 1 }));
    }

    this.rowReset.emit(row);
  }

  /** Handles the submit event of specified row. For example when users press key Enter */
  submitRow(row: MatxRow): void {
    if (row.form?.invalid) {
      return;
    }
    // TODO: add row submitted async
    this.columns.forEach((c) => {
      _set(row.item, c.field, row.form?.get(c.field)?.value);
      row.form?.removeControl(c.field);
    });
    delete row.form;
    row.editing = false;
    row.creating = false;
    row.loading = false;
  }

  toggleColumnsFiltering(): void {
    if (this.showColumnsFilters) this.endColumnsFiltering();
    else this.startColumnsFiltering();
  }

  /** Shows the filters row and starts filtering the table */
  private startColumnsFiltering(): void {
    this.columnsFiltersForm = new FormGroup({});
    this.columns.forEach((c) =>
      this.columnsFiltersForm.addControl(
        c.field,
        c.comparison === '$between'
          ? new FormGroup({ min: new FormControl(), max: new FormControl() })
          : new FormControl()
      )
    );
    this.showColumnsFilters = true;
    this.columnsFiltersChangeSubs = this.columnsFiltersForm.valueChanges
      .pipe(debounceTime(500))
      .subscribe(() => this.filterRows());
  }

  /** Resets the filters and hides the filters row */
  private endColumnsFiltering(): void {
    this.columnsFiltersForm.reset();
    this.rows = [...this.clonedRows];
    this.showColumnsFilters = false;
    this.columnsFiltersChangeSubs.unsubscribe();
  }

  /** Applies the columns filters to the rows if the data is local, else it emits the filter event containing the filters to be applied */
  applyColumnsFilter() {
    if (!this.columns) return;
    const columns = Array.from(this.columns);
    this._columnsQuery =
      this.columnsFiltersForm &&
      columns.reduce((cv, c) => {
        const value = this.columnsFiltersForm.controls[c.field].value;
        return {
          ...cv,
          ...(c.comparison === '$between'
            ? (value.min || value.max) && {
                [c.field]: {
                  [c.comparison]: [
                    (value.min && Number(value.min)) ||
                      Number.NEGATIVE_INFINITY,
                    (value.max && Number(value.max)) ||
                      Number.POSITIVE_INFINITY,
                  ],
                },
              }
            : c.comparison === '$in'
            ? value?.length
              ? { [c.field]: { $in: value } }
              : null
            : value && { [c.field]: { [c.comparison]: value } }),
        };
      }, {});

    if (this.remote || this.dataChangeSubs) {
      this.columnsQueryChange.emit(this._columnsQuery);
      return;
    }

    this.filterRows();
  }

  refreshData() {
    this.refreshClicked.emit();
  }

  /** Filters the rows if the data is local, else it emits the filter event containing the filters to be applied */
  private filterRows() {
    if (!this.columns) return;
    const columns = Array.from(this.columns);

    const searchBoxQuery = columns.reduce(
      (cv, c) => ({
        ...cv,
        [c.field]: { $likeI: this.searchBoxControl.value },
      }),
      {}
    );

    const query = {
      ...this._columnsQuery,
      ...(this.searchBoxControl.value && { $or: searchBoxQuery }),
    };

    this.filteredRows = this.clonedRows.filter((r) => where(query)(r.item));

    this._sortRows();
    this.totalItems.emit(this.filteredRows.length);
  }

  /** Sorts the table with respect to the selected columns */
  _sort(column: MatxColumnComponent): void {
    if (!column.sortable) {
      return;
    }

    column.sortDirection =
      column.sortDirection === 0 ? 1 : column.sortDirection === 1 ? -1 : 0;

    if (column.sortDirection !== 0) {
      if (!this.sortedColumnsSet.has(column)) {
        this.sortedColumnsSet.add(column);
        this.sortedColumns.push(column);
      }
    } else {
      this.sortedColumnsSet.delete(column);
      this.sortedColumns.pop();
    }

    if (this.dataChangeSubs || this._remote) {
      const orderBy = this.sortedColumns
        .map((c) => c.sortBy + ' ' + (c.sortDirection > 0 ? 'asc' : 'desc'))
        .join(',');
      this.sort.emit(orderBy);
      return;
    }
    this._sortRows();
  }

  private _sortRows(): void {
    this.rows = [...(this.filteredRows || this.clonedRows)];
    if (this.sortedColumns.length === 0) {
      return;
    }
    const fields = this.sortedColumns.map((c) =>
      typeof c.sortBy === 'function' ? c.sortBy : 'item.' + c.sortBy
    );
    this.rows.sort(
      compare(
        fields,
        this.sortedColumns.map((c) => c.sortDirection)
      )
    );
  }

  /** Gets the csv string of the printable table */
  async getCsv(linkColumnIndex = -1): Promise<string> {
    this.renderExportable = true;

    const parser = linkColumnIndex >= 0 ? new DOMParser() : null;
    const printable = await this.getPrintableElement();
    const result = Array.from(printable.querySelectorAll('tr'))
      .map((r) =>
        Array.from(r.querySelectorAll('th,td'))
          .map((c, index) => {
            // return c.textContent.includes(',') ? `"${c.textContent}"` : c.textContent
            let textContent = c.textContent;
            if (
              linkColumnIndex === index &&
              textContent &&
              textContent.length > 0
            ) {
              const document = parser?.parseFromString(
                textContent,
                'text/html'
              );
              const anchorElements = document?.getElementsByTagName('a');
              if (anchorElements?.length === 1) {
                // handle link
                const anchor: HTMLAnchorElement = anchorElements[0];
                const hRef = anchor.href;
                const text = anchor.innerText;
                textContent = `${hRef}#${text}`;
              }
            }
            textContent = textContent?.replace(/"/gi, '""') ?? ''; // replace " with ""
            return `"${textContent}"`; // surround textContent with "", beginning and end
          })
          .join(',')
      )
      .join('\n');

    this.renderExportable = false;
    return result;
  }

  /** Gets the html element of the printable table */
  async getPrintableElement(): Promise<HTMLElement> {
    this.renderPrintable = true;
    await firstValueFrom(of(null).pipe(delay(200)));
    const result = this._printable.nativeElement;
    this.renderPrintable = false;
    return result;
  }

  /** Gets the html string of the printable table */
  async getPrintable(): Promise<string> {
    return (await this.getPrintableElement()).innerHTML;
  }

  private updateTableRowStyling(tableRow: ChildNode): void {
    tableRow.childNodes.forEach((cell: any) => {
      const localName = cell.localName;
      if (localName === 'th' || localName === 'td') {
        // apply style of <th/tr><div..</div><th/tr> to <th/tr ..><th/tr>

        const childClasses: DOMTokenList = cell.firstElementChild?.classList;
        if (childClasses != null && childClasses.length > 0) {
          let cellClasses: DOMTokenList = cell?.classList;
          if (cellClasses == null) {
            cellClasses = new DOMTokenList();
          }
          childClasses.forEach((cc) => {
            if (!cellClasses.contains(cc)) {
              cellClasses.add(cc);
            }
          });
        }
        const childStyleValue =
          cell.firstElementChild?.attributes.style?.nodeValue;
        if (childStyleValue) {
          const cellStyleValue = cell.attributes
            ? cell.attributes.style?.nodeValue
            : '';
          const styleValue = `${cellStyleValue ? cellStyleValue + '; ' : ''}${
            childStyleValue ?? ''
          }`;
          if (styleValue !== '' && cell.setAttribute) {
            cell.setAttribute('style', styleValue);
          }
        }
      }
    });
  }

  /** Opens a preview window */
  async preview(): Promise<Window> {
    const pw = window.open() as Window;
    const htmlElement = await this.getPrintableElement();
    const computedStyle = window.getComputedStyle(htmlElement);
    const fontFamily = computedStyle?.fontFamily ?? '';
    const fontSize = computedStyle?.fontSize ?? '';
    const fontWeight = computedStyle?.fontWeight ?? '';
    const title = htmlElement
      .querySelector('[xlstile]')
      ?.getAttribute('xlstile');
    const childNodes = htmlElement.childNodes;
    childNodes.forEach((table) => {
      const tableStyle = (table as HTMLTableElement).style;
      if (computedStyle != null && tableStyle != null) {
        tableStyle.fontFamily = fontFamily;
        tableStyle.fontSize = fontSize;
        tableStyle.fontWeight = fontWeight;
      }
      const rows = table.childNodes;
      rows.forEach((row) => {
        if ((row as HTMLTableRowElement).localName === 'tr') {
          this.updateTableRowStyling(row);
        }
      });
    });

    const titleElement = title
      ? `<div class="matx-cell-title"><span>${title}<span></div>`
      : '';
    const printableElement = titleElement + htmlElement.innerHTML;
    // let printableElement = await this.getPrintable();
    const result = `<html><head>
${
  this.printStyleUrl
    ? `<link rel="stylesheet" href="${this.printStyleUrl}">`
    : ''
}
<style>${this.printStyleCss}</style>
</head><body>${printableElement}</body></html>`;
    pw?.document?.write(result);
    pw?.document?.close();
    return pw;
  }

  /** Opens a new windows asking for printing the table */
  async print(): Promise<void> {
    const pw = await this.preview();
    pw.print();
  }

  /** Opens a new windows asking for printing the table and close it after finishing */
  async printAndClose(): Promise<void> {
    const pw = await this.preview();
    pw.print();
    pw.close();
  }

  /** Copy table to csv */
  async copyCsv() {
    this.clipboard.copy(await this.getCsv());
  }

  _includesLeftButtons() {
    return (
      this.controls === 'all' ||
      this.controls.includes('print-button') ||
      this.controls.includes('copy-csv-button') ||
      this.controls.includes('view-more-button')
    );
  }

  viewMoreClick() {
    this.viewMoreClicked.emit();
  }

  getScrollLeft(event: Event) {
    return (event.target as HTMLElement).scrollLeft;
  }

  _rowDropped(event: CdkDragDrop<MatxRow[], MatxRow>) {
    const vsStartIndex = this.virtualScroller.getRenderedRange().start;
    event.currentIndex += vsStartIndex;
    event.previousIndex += vsStartIndex;
    if (this.reorderableRows === 'external') {
      this.rowDropped.emit(event);
      return;
    }
    moveItemInArray(this.rows, event.previousIndex, event.currentIndex);
  }
}
