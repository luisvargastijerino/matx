import { FormGroup } from '@angular/forms';

export interface MatxRow {
  item: any;
  editing: boolean;
  creating: boolean;
  expanded?: boolean;
  form?: FormGroup | any;
  children?: any[];
  parent?: MatxRow;
  loading?: boolean;
  index: number;
}
