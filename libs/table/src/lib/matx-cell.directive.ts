import { Directive } from '@angular/core';

/**
 * Template used to create cell containing custom html components:
 *
 * ```html
 * <matx-column>
 *   <ng-template matxCell let-player="item"><b>{{player.name}}</b></ng-template>
 * </matx-column>
 * ```
 *
 * or:
 * ```html
 * <matx-column>
 *   <div *dtCell="let player=item"><b>{{player.name}}</b></ng-template>
 * </matx-column>
 * ```
 */
@Directive({selector: 'ng-template[matxCell]'})
export class MatxCellDirective {}
