import { Directive } from '@angular/core';

@Directive({selector: 'ng-template[matxRowDetail]'})
export class MatxRowDetailDirective {}
