import { Directive } from '@angular/core';

@Directive({selector: 'ng-template[dtExportable]'})
export class MatxExportableDirective {}
