import { Directive } from '@angular/core';

@Directive({
  selector: 'ng-template[dtCustomFilters]'
})
export class MatxCustomFiltersDirective {}
