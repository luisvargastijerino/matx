import { Directive, ElementRef, Host, HostListener, Input, OnInit, SkipSelf } from '@angular/core';
import { MatxColumnComponent } from './matx-column.component';

@Directive({
  selector: '[MatxColumnResizable]'
})
export class MatxColumnResizableDirective implements OnInit {
  @Input() column!: MatxColumnComponent;

  _host!: HTMLElement;
  _startWidth = 0;
  _minWidth = 35;
  _tempWidth = '';
  constructor(private elm: ElementRef) { }

  ngOnInit() {
    this._host = this.elm.nativeElement;
    if (this.column.minWidth) {
      this._minWidth = Number(this.column.minWidth.replace('px', ''));
    }
  }

  dragStart() {
   this.toggleDragging();

    document.body.style.cursor = "ew-resize";

    this._startWidth = this._host.offsetWidth;
    if (this._startWidth < this._minWidth) {
      this._startWidth = this._minWidth;
    }
  }

  dragging(diff: number) {
    if (this._minWidth < this._startWidth - diff) {
      this.elm.nativeElement.style.flex = '0 0 ' + (this._startWidth - diff) + 'px';
      //this.column.width = (this._startWidth - diff) + 'px';
      this._tempWidth = (this._startWidth - diff) + 'px';
    }
  }

  dragEnd() {

    document.body.style.cursor = "";

    this.column.width = this._tempWidth;

    this.toggleDragging();

    //this.column.width = this.elm.nativeElement.style.width;
    this._startWidth = 0;
  }

  private toggleDragging() {
    const overlay = document.getElementsByClassName("matx-column-sizer");

    for (let i = 0; i < overlay.length; i++) {
      overlay[i].classList.toggle('not-draggable');
    }

    const theGrabber = this.elm.nativeElement.getElementsByClassName("matx-column-sizer")[0];
    theGrabber.classList.toggle('not-draggable');
    theGrabber.classList.toggle('dragging');
  }
}
