import { Directive } from '@angular/core';

@Directive({
  selector: '[matxColumnFilter]'
})
export class MatxColumnFilterDirective {}
