import { Directive, ElementRef, Host, HostListener, SkipSelf } from '@angular/core';
import { MatxColumnResizableDirective } from './matx-column-resizable-directive';
import _throttle from 'lodash-es/throttle';


@Directive({
  selector: '[matxColumnResizeGrabber]',
})
export class MatxColumnResizeGrabberDirective {

  constructor(
    private elm: ElementRef,
    @Host() @SkipSelf() private resizable: MatxColumnResizableDirective,
  ) { }

  private _startOffsetX?: number;

  private _dragging = _throttle((e: MouseEvent) => {
    if (this._startOffsetX === undefined) { return; } // This avoids glitch of mousemove after mouseup
    const diff = this._startOffsetX - e.clientX;
    this.resizable.dragging(diff);
  }, 10);

  @HostListener('mousedown', ['$event'])
  mousedown(e: MouseEvent) {
    if (e.button !== 0) return;

    this._startOffsetX = e.clientX;
    document.addEventListener('mousemove', this._dragging);
    document.addEventListener('mouseup', this._dragEnd);
    this.resizable.dragStart();
  }

  private _dragEnd = (e: MouseEvent) => {
    this._startOffsetX = undefined;
    document.removeEventListener('mousemove', this._dragging);
    document.removeEventListener('mouseup', this._dragEnd);
    this.resizable.dragEnd();
  }
}
