
export interface MatxSortEvent {
  sortBy: string;
  sortDirection: string;
}
