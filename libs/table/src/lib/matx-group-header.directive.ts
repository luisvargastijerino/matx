import { Directive } from '@angular/core';

@Directive({selector: '[matxGroupHeader]'})
export class MatxGroupHeaderDirective {}
