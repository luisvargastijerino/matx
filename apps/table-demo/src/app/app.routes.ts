import { Route } from '@angular/router';

export const appRoutes: Route[] = [
  { path: '', redirectTo: 'basic', pathMatch: 'full' },
  {
    path: 'basic',
    loadComponent: () =>
      import('./pages/basic/basic.component').then((m) => m.BasicComponent),
  },
  {
    path: 'basic-remote',
    loadComponent: () =>
      import('./pages/basic-remote/basic-remote.component').then(
        (m) => m.BasicRemoteComponent
      ),
  },
  {
    path: 'selectable',
    loadComponent: () =>
      import('./pages/selectable/selectable.component').then(
        (m) => m.SelectableComponent
      ),
  },
  {
    path: 'scrollable',
    loadComponent: () =>
      import('./pages/scrollable/scrollable.component').then(
        (m) => m.ScrollableComponent
      ),
  },
  {
    path: 'frozen-columns',
    loadComponent: () =>
      import('./pages/frozen-columns/frozen-columns.component').then(
        (m) => m.FrozenColumnsComponent
      ),
  },
  {
    path: 'sortable',
    loadComponent: () =>
      import('./pages/sortable/sortable.component').then(
        (m) => m.SortableComponent
      ),
  },
  {
    path: 'sortable-remote',
    loadComponent: () =>
      import('./pages/sortable-remote/sortable-remote.component').then(
        (m) => m.SortableRemoteComponent
      ),
  },
  {
    path: 'cell-template',
    loadComponent: () =>
      import('./pages/cell-template/cell-template.component').then(
        (m) => m.CellTemplateComponent
      ),
  },
  {
    path: 'editor-template',
    loadComponent: () =>
      import('./pages/editor-template/editor-template.component').then(
        (m) => m.EditorTemplateComponent
      ),
  },
  {
    path: 'row-detail-template',
    loadComponent: () =>
      import('./pages/row-detail-template/row-detail-template.component').then(
        (m) => m.RowDetailTemplateComponent
      ),
  },
  {
    path: 'group-by',
    loadComponent: () =>
      import('./pages/group-by/group-by.component').then(
        (m) => m.GroupByComponent
      ),
  },
  {
    path: 'column-filter',
    loadComponent: () =>
      import('./pages/column-filter/column-filter.component').then(
        (m) => m.ColumnFilterComponent
      ),
  },
  {
    path: 'column-filter-remote',
    loadComponent: () =>
      import(
        './pages/column-filter-remote/column-filter-remote.component'
      ).then((m) => m.ColumnFilterRemoteComponent),
  },
  {
    path: 'print',
    loadComponent: () =>
      import('./pages/print/print.component').then((m) => m.PrintComponent),
  },
];
