import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FrozenColumnsRoutingModule } from './frozen-columns-routing.module';
import { FrozenColumnsComponent } from './frozen-columns.component';
import { MatxTableModule } from 'matx-table';


@NgModule({
  declarations: [FrozenColumnsComponent],
  imports: [
    CommonModule,
    FrozenColumnsRoutingModule,
    MatxTableModule
  ]
})
export class FrozenColumnsModule { }
