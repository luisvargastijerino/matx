import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SortableRoutingModule } from './sortable-routing.module';
import { SortableComponent } from './sortable.component';
import { MatxTableModule } from 'matx-table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatIconModule } from '@angular/material/icon';


@NgModule({
  declarations: [SortableComponent],
    imports: [
        CommonModule,
        SortableRoutingModule,
        MatxTableModule,
        MatPaginatorModule,
        MatIconModule
    ]
})
export class SortableModule { }
