import { Component } from '@angular/core';
import { MatxInputComponent, MatxSelectComponent } from 'matx-core';
import {
  MatxCellDirective,
  MatxColumnComponent,
  MatxEditorDirective,
  MatxTableComponent,
} from 'matx-table';
import { ReactiveFormsModule } from '@angular/forms';
import { MatMenu, MatMenuItem, MatMenuTrigger } from '@angular/material/menu';
import { MatIcon } from '@angular/material/icon';
import { MatIconButton } from '@angular/material/button';

@Component({
  selector: 'app-editor-template',
  templateUrl: './editor-template.component.html',
  imports: [
    MatxInputComponent,
    MatxTableComponent,
    MatxColumnComponent,
    ReactiveFormsModule,
    MatxSelectComponent,
    MatMenuTrigger,
    MatIcon,
    MatMenu,
    MatMenuItem,
    MatxEditorDirective,
    MatIconButton,
    MatxCellDirective,
  ],
  styleUrls: ['./editor-template.component.scss'],
})
export class EditorTemplateComponent {
  players = [
    {
      id: 1,
      age: 27,
      club: 'Barcelona',
      name: 'Leonel Messi',
      national: 'Argentina',
    },
    {
      id: 2,
      age: 29,
      club: 'Real Madrid',
      name: 'Cristiano Ronaldo',
      national: 'Portugal',
    },
    {
      id: 3,
      age: 34,
      club: 'Liverpool',
      name: 'Steven Gerrard',
      national: 'England',
    },
    { id: 4, age: 22, club: 'Barcelona', name: 'Neymar', national: 'Brazil' },
    {
      id: 5,
      age: 25,
      club: 'Borussia Dortmund',
      name: 'Marco Reus',
      national: 'Germany',
    },
    {
      id: 6,
      age: 26,
      club: 'Real Madrid',
      name: 'Karim Benzema',
      national: 'France',
    },
    {
      id: 7,
      age: 30,
      club: 'Manchester United',
      name: 'Robin Van Persie',
      national: 'Holland',
    },
    {
      id: 8,
      age: 28,
      club: 'Manchester City',
      name: 'David Silva',
      national: 'Spain',
    },
    {
      id: 9,
      age: 35,
      club: 'Juventus',
      name: 'Andrea Pirlo',
      national: 'Italy',
    },
    {
      id: 10,
      age: 34,
      club: 'Juventus',
      name: 'Andrea Pirlo',
      national: 'Italy',
    },
    {
      id: 11,
      age: 34,
      club: 'Real Madrid',
      name: 'Andrea Pirlo',
      national: 'Italy',
    },
  ];
}
