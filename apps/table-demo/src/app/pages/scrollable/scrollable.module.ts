import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ScrollableRoutingModule } from './scrollable-routing.module';
import { ScrollableComponent } from './scrollable.component';
import { MatxTableModule } from 'matx-table';


@NgModule({
  declarations: [ScrollableComponent],
  imports: [
    CommonModule,
    ScrollableRoutingModule,
    MatxTableModule
  ]
})
export class ScrollableModule { }
