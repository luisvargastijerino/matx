import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SelectableRoutingModule } from './selectable-routing.module';
import { SelectableComponent } from './selectable.component';
import { MatxTableModule } from 'matx-table';
import { MatCheckboxModule } from '@angular/material/checkbox';


@NgModule({
  declarations: [SelectableComponent],
    imports: [
        CommonModule,
        SelectableRoutingModule,
        MatxTableModule,
        MatCheckboxModule
    ]
})
export class SelectableModule { }
