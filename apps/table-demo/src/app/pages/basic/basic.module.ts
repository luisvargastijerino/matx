import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BasicRoutingModule } from './basic-routing.module';
import { BasicComponent } from './basic.component';
import { MatxTableModule } from 'matx-table';


@NgModule({
  declarations: [BasicComponent],
  imports: [
    CommonModule,
    BasicRoutingModule,
    MatxTableModule
  ]
})
export class BasicModule { }
