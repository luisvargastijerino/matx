import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PrintRoutingModule } from './print-routing.module';
import { PrintComponent } from './print.component';
import { MatxTableModule } from 'matx-table';
import { MatButtonModule } from '@angular/material/button';


@NgModule({
  declarations: [PrintComponent],
  imports: [
    CommonModule,
    PrintRoutingModule,
    MatxTableModule,
    MatButtonModule
  ]
})
export class PrintModule { }
