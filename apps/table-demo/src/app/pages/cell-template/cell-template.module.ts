import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CellTemplateRoutingModule } from './cell-template-routing.module';
import { CellTemplateComponent } from './cell-template.component';
import { MatxTableModule } from 'matx-table';


@NgModule({
  declarations: [CellTemplateComponent],
  imports: [
    CommonModule,
    CellTemplateRoutingModule,
    MatxTableModule
  ]
})
export class CellTemplateModule { }
