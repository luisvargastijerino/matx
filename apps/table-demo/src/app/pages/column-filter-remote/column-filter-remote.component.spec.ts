import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ColumnFilterRemoteComponent } from './column-filter-remote.component';

describe('ColumnFilterComponent', () => {
  let component: ColumnFilterRemoteComponent;
  let fixture: ComponentFixture<ColumnFilterRemoteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ColumnFilterRemoteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ColumnFilterRemoteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
