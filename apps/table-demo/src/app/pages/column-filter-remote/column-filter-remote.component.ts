import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { QMatcher, QStringMatcher, Query } from 'array-query-ts';
import { MatxCellDirective, MatxColumnComponent, MatxColumnFilterDirective, MatxTableComponent } from 'matx-table';
import { MatxInputComponent } from 'matx-core';
import { ReactiveFormsModule } from '@angular/forms';

interface User {
  id: number;
  name: string;
  username: string;
  email: string;
}

interface Post {
  id: number;
  title: string;
  body: string;
  user: User;
}

@Component({
  selector: 'app-column-filter',
  templateUrl: './column-filter-remote.component.html',
  imports: [
    MatxTableComponent,
    MatxColumnComponent,
    MatxInputComponent,
    ReactiveFormsModule,
    MatPaginator,
    MatxColumnFilterDirective,
    MatxCellDirective,
  ],
  styleUrls: ['./column-filter-remote.component.scss'],
})
export class ColumnFilterRemoteComponent {
  posts: Post[] = [];

  page = {
    total: 0,
    size: 10,
    number: 1,
  };
  loading = true;

  searchText = '';
  columnsQuery = {} as {
    title?: QStringMatcher;
    'user.username'?: QStringMatcher;
  };

  constructor(private http: HttpClient) {}

  ngOnInit(): void {
    this.getPosts();
  }

  searchByText(searchText: string) {
    console.log('searchText: ', searchText);
    this.searchText = searchText;
    this.page.number = 1;
    this.getPosts();
  }

  searchByColumns(columnsQuery: Query<any>) {
    console.log('columnsQuery: ', columnsQuery);
    this.columnsQuery = columnsQuery as this['columnsQuery'];
    this.getPosts();
  }

  changePage(page: PageEvent) {
    console.log('page: ', page);
    this.page.size = page.pageSize;
    this.page.number = page.pageIndex + 1;
    this.getPosts();
  }

  getPosts(): void {
    this.loading = true;
    this.http
      .get<Post[]>('https://jsonplaceholder.typicode.com/posts', {
        observe: 'response',
        params: {
          _expand: 'user',
          _page: this.page.number.toString(),
          _limit: this.page.size.toString(),
          ...(!this.searchText ? {} : { q: this.searchText }),
          ...(!this.columnsQuery['title']
            ? {}
            : {
                title_like: this.columnsQuery.title?.$likeI,
              }),
          ...(!this.columnsQuery['user.username']
            ? {}
            : {
                'user.username_like':
                  this.columnsQuery['user.username']?.$likeI,
              }),
        },
      })
      .subscribe((resp) => {
        this.loading = false;
        this.page.total = Number(resp.headers.get('x-total-count'));
        this.posts = resp.body ?? [];
      });
  }
}
