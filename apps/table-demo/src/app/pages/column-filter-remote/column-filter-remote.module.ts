import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ColumnFilterRemoteRoutingModule } from './column-filter-remote-routing.module';
import { ColumnFilterRemoteComponent } from './column-filter-remote.component';
import { MatxTableModule } from 'matx-table';
import { MatxInputModule, MatxSelectModule } from 'matx-core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatIconModule } from '@angular/material/icon';
import { MatPaginatorModule } from '@angular/material/paginator';
import { HttpClientModule } from '@angular/common/http';


@NgModule({
  declarations: [ColumnFilterRemoteComponent],
  imports: [
    CommonModule,
    HttpClientModule,
    ColumnFilterRemoteRoutingModule,
    MatxTableModule,
    MatxInputModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatxSelectModule,
    MatIconModule,
    MatPaginatorModule
  ]
})
export class ColumnFilterRemoteModule { }
