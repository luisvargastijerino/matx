import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ColumnFilterRemoteComponent } from './column-filter-remote.component';

const routes: Routes = [{ path: '', component: ColumnFilterRemoteComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ColumnFilterRemoteRoutingModule { }
