declare const process: {
  env: {
    NG_APP_ENV: string;
    NG_APP_GMAP_KEY: string;
  };
};
