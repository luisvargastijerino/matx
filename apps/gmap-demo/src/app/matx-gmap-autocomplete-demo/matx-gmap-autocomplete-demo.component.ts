import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  FormsModule,
  ReactiveFormsModule,
} from '@angular/forms';
import { MatxGmapAutocompleteComponent } from 'matx-gmap';
import { MatxErrorsComponent } from 'matx-core';
import { JsonPipe } from '@angular/common';
import { GOOGLE_MAPS_API_CONFIG, NgMapsGoogleModule } from '@ng-maps/google';
import { environment } from '../../environments/environment';
import { NgMapsCoreModule } from '@ng-maps/core';

@Component({
  selector: 'matx-gmap-autocomplete-demo',
  imports: [
    MatxGmapAutocompleteComponent,
    FormsModule,
    MatxErrorsComponent,
    JsonPipe,
    ReactiveFormsModule,
    NgMapsGoogleModule,
    NgMapsCoreModule,
  ],
  templateUrl: './matx-gmap-autocomplete-demo.component.html',
})
export class MatxGmapAutocompleteDemoComponent implements OnInit {
  form1Model = {
    gmapInput1: { address: 'test1' },
  };

  form2!: FormGroup;

  constructor(private fb: FormBuilder) {}

  ngOnInit(): void {
    this.form2 = this.fb.group({
      gmapInput2: { address: 'test2' },
    });
  }
}
