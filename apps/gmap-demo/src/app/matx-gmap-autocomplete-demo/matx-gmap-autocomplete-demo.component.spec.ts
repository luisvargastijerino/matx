import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MatxGmapAutocompleteDemoComponent } from './matx-gmap-autocomplete-demo.component';

describe('MatxGmapAutocompleteDemoComponent', () => {
  let component: MatxGmapAutocompleteDemoComponent;
  let fixture: ComponentFixture<MatxGmapAutocompleteDemoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MatxGmapAutocompleteDemoComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(MatxGmapAutocompleteDemoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
