import { Component } from '@angular/core';
import { MatxMenuButtonComponent, MatxNavTreeComponent, MatxNavTreeItem, MatxSidenavMenuService } from 'matx-core';
import { NavigationEnd, Router, RouterOutlet } from '@angular/router';
import { MatSidenav, MatSidenavContainer } from '@angular/material/sidenav';
import { MatToolbar } from '@angular/material/toolbar';
import { MatTab, MatTabGroup } from '@angular/material/tabs';
import { Highlight } from 'ngx-highlightjs';

@Component({
  selector: 'matx-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  imports: [
    MatSidenavContainer,
    MatSidenav,
    MatToolbar,
    MatxNavTreeComponent,
    MatxMenuButtonComponent,
    MatTabGroup,
    MatTab,
    RouterOutlet,
    Highlight,
  ],
})
export class AppComponent {
  menuItems: MatxNavTreeItem[] = [
    { displayName: 'Autocomplete', route: '/autocomplete' },
  ];

  sources: {
    [url: string]: { html: { default: string }; ts: { default: string } };
  } = {
    '/autocomplete': {
      html: require('!raw-loader!./matx-gmap-autocomplete-demo/matx-gmap-autocomplete-demo.component.html'),
      ts: require('!raw-loader!./matx-gmap-autocomplete-demo/matx-gmap-autocomplete-demo.component.ts'),
    },
  };

  currentUrl = '';

  constructor(router: Router, public sidenavMenuCtrl: MatxSidenavMenuService) {
    router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        this.currentUrl = router.url;
      }
    });
  }

  getDisplayName(menuItems: MatxNavTreeItem[], currentUrl: string) {
    return menuItems.find((mi) => mi.route === currentUrl)?.displayName ?? '';
  }
}
