import { Route } from '@angular/router';

export const appRoutes: Route[] = [
  { path: '', pathMatch: 'full', redirectTo: '/autocomplete' },
  {
    path: 'autocomplete',
    loadComponent: () =>
      import(
        './matx-gmap-autocomplete-demo/matx-gmap-autocomplete-demo.component'
      ).then((m) => m.MatxGmapAutocompleteDemoComponent),
  },
];
