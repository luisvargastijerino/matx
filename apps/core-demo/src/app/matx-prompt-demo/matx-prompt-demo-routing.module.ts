import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MatxPromptDemoComponent } from './matx-prompt-demo.component';

const routes: Routes = [{path: '', component: MatxPromptDemoComponent}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MatxPromptDemoRoutingModule { }
