import { Component } from '@angular/core';
import { NavigationEnd, Router, RouterOutlet } from '@angular/router';
import {
  MatxMenuButtonComponent,
  MatxNavTreeComponent,
  MatxNavTreeItem,
  MatxSidenavMenuService,
} from 'matx-core';
import { MatSidenav, MatSidenavContainer, MatSidenavContent } from '@angular/material/sidenav';
import { MatToolbar } from '@angular/material/toolbar';
import { MatTab, MatTabGroup } from '@angular/material/tabs';
import { Highlight } from 'ngx-highlightjs';

@Component({
  selector: 'matx-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  standalone: true,
  imports: [
    MatSidenavContainer,
    MatSidenavContent,
    MatSidenav,
    MatToolbar,
    MatxNavTreeComponent,
    MatxMenuButtonComponent,
    MatTabGroup,
    MatTab,
    RouterOutlet,
    Highlight,
  ],
})
export class AppComponent {
  menuItems: MatxNavTreeItem[] = [
    { displayName: 'Input', route: '/matx-input' },
    { displayName: 'Datepicker', route: '/matx-datepicker' },
    { displayName: 'Autocomplete', route: '/matx-autocomplete' },
    { displayName: 'Back-Button', route: '/matx-back-button' },
    { displayName: 'Menu-Button', route: '/matx-menu-button' },
    { displayName: 'Select', route: '/matx-select' },
    { displayName: 'Prompt', route: '/matx-prompt' },
    { displayName: 'Nav Tree', route: '/matx-nav-tree' },
  ];

  sources: {
    [
      url: string
    ]: { html: { default: string }; ts: { default: string } };
  } = {
    '/matx-input': {
      html: require('!raw-loader!./matx-input-demo/matx-input-demo.component.html'),
      ts: require('!raw-loader!./matx-input-demo/matx-input-demo.component.ts'),
    },
    '/matx-datepicker': {
      html: require('!raw-loader!./matx-datepicker-demo/matx-datepicker-demo.component.html'),
      ts: require('!raw-loader!./matx-datepicker-demo/matx-datepicker-demo.component.ts'),
    },
    '/matx-autocomplete': {
      html: require('!raw-loader!./matx-autocomplete-demo/matx-autocomplete-demo.component.html'),
      ts: require('!raw-loader!./matx-autocomplete-demo/matx-autocomplete-demo.component.ts'),
    },
    '/matx-back-button': {
      html: require('!raw-loader!./matx-back-button-demo/matx-back-button-demo.component.html'),
      ts: require('!raw-loader!./matx-back-button-demo/matx-back-button-demo.component.ts'),
    },
    '/matx-menu-button': {
      html: require('!raw-loader!./matx-menu-button-demo/matx-menu-button-demo.component.html'),
      ts: require('!raw-loader!./matx-menu-button-demo/matx-menu-button-demo.component.ts'),
    },
    '/matx-select': {
      html: require('!raw-loader!./matx-select-demo/matx-select-demo.component.html'),
      ts: require('!raw-loader!./matx-select-demo/matx-select-demo.component.ts'),
    },
    '/matx-prompt': {
      html: require('!raw-loader!./matx-prompt-demo/matx-prompt-demo.component.html'),
      ts: require('!raw-loader!./matx-prompt-demo/matx-prompt-demo.component.ts'),
    },
    '/matx-nav-tree': {
      html: require('!raw-loader!./matx-nav-tree-demo/matx-nav-tree-demo.component.html'),
      ts: require('!raw-loader!./matx-nav-tree-demo/matx-nav-tree-demo.component.ts'),
    },
  };

  currentUrl = '';

  constructor(router: Router, public sidenavMenuSvc: MatxSidenavMenuService) {
    router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        this.currentUrl = router.url.replace(/\/nav-.+/, '');
      }
    });
  }
}
