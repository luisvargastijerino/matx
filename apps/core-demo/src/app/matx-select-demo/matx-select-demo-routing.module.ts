import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MatxSelectDemoComponent } from './matx-select-demo.component';

const routes: Routes = [{path: '', component: MatxSelectDemoComponent}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MatxSelectDemoRoutingModule { }
