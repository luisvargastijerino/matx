import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MatxAutocompleteDemoComponent } from './matx-autocomplete-demo.component';

const routes: Routes = [{ path: '', component: MatxAutocompleteDemoComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MatxAutocompleteDemoRoutingModule { }
