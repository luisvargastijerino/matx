import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MatxDatepickerDemoComponent } from './matx-datepicker-demo.component';

const routes: Routes = [{ path: '', component: MatxDatepickerDemoComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MatxDatepickerDemoRoutingModule {}
