import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatxDatepickerComponent, MatxErrorsComponent } from 'matx-core';
import { MatIcon } from '@angular/material/icon';
import { JsonPipe } from '@angular/common';
import { MatPrefix, MatSuffix } from '@angular/material/form-field';

@Component({
  selector: 'matx-input-demo',
  imports: [
    MatxDatepickerComponent,
    MatIcon,
    FormsModule,
    MatxErrorsComponent,
    JsonPipe,
    ReactiveFormsModule,
    MatPrefix,
    MatSuffix,
  ],
  templateUrl: './matx-datepicker-demo.component.html',
})
export class MatxDatepickerDemoComponent implements OnInit {
  form1Model = {
    matxDatepicker1: null,
  };

  form2!: FormGroup;

  constructor(private fb: FormBuilder) {}

  ngOnInit(): void {
    this.form2 = this.fb.group({
      matxDatepicker2: null,
    });
  }
}
