import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MatxDatepickerDemoComponent } from './matx-datepicker-demo.component';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { provideNativeDateAdapter } from '@angular/material/core';

describe('MatxDatepickerDemoComponent', () => {
  let component: MatxDatepickerDemoComponent;
  let fixture: ComponentFixture<MatxDatepickerDemoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [MatxDatepickerDemoComponent, NoopAnimationsModule],
      providers: [provideNativeDateAdapter()]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MatxDatepickerDemoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
