import { Route } from '@angular/router';

export const appRoutes: Route[] = [
  { path: '', pathMatch: 'full', redirectTo: '/matx-input' },
  {
    path: 'matx-input',
    loadComponent: () =>
      import('./matx-input-demo/matx-input-demo.component').then(
        (m) => m.MatxInputDemoComponent
      ),
  },
  {
    path: 'matx-datepicker',
    loadComponent: () =>
      import('./matx-datepicker-demo/matx-datepicker-demo.component').then(
        (m) => m.MatxDatepickerDemoComponent
      ),
  },
  {
    path: 'matx-autocomplete',
    loadComponent: () =>
      import('./matx-autocomplete-demo/matx-autocomplete-demo.component').then(
        (m) => m.MatxAutocompleteDemoComponent
      ),
  },
  {
    path: 'matx-back-button',
    loadComponent: () =>
      import('./matx-back-button-demo/matx-back-button-demo.component').then(
        (m) => m.MatxBackButtonDemoComponent
      ),
  },
  {
    path: 'matx-menu-button',
    loadComponent: () =>
      import('./matx-menu-button-demo/matx-menu-button-demo.component').then(
        (m) => m.MatxMenuButtonDemoComponent
      ),
  },
  {
    path: 'matx-select',
    loadComponent: () =>
      import('./matx-select-demo/matx-select-demo.component').then(
        (m) => m.MatxSelectDemoComponent
      ),
  },
  {
    path: 'matx-prompt',
    loadComponent: () =>
      import('./matx-prompt-demo/matx-prompt-demo.component').then(
        (m) => m.MatxPromptDemoComponent
      ),
  },
  {
    path: 'matx-nav-tree',
    loadChildren: () =>
      import('./matx-nav-tree-demo/matx-nav-tree-demo.routes').then(
        (r) => r.MATX_NAV_TREE_DEMO_ROUTES
      ),
  },
];
