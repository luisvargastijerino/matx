import { Routes } from '@angular/router';
import { MatxNavTreeDemoComponent } from './matx-nav-tree-demo.component';

export const MATX_NAV_TREE_DEMO_ROUTES: Routes = [
  {
    path: '',
    component: MatxNavTreeDemoComponent,
    children: [
      {
        path: 'nav-1',
        loadComponent: () =>
          import('./nav-1/nav-1.component').then((m) => m.Nav1Component),
      },
      {
        path: 'nav-1/1',
        loadComponent: () =>
          import('./nav-1-1/nav-1-1.component').then((m) => m.Nav11Component),
      },
      {
        path: 'nav-1/1/1',
        loadComponent: () =>
          import('./nav-1-1-1/nav-1-1-1.component').then((m) => m.Nav111Component),
      },
      {
        path: 'nav-1/2',
        loadComponent: () =>
          import('./nav-1-2/nav-1-2.component').then((m) => m.Nav12Component),
      },
      {
        path: 'nav-1/2/1',
        loadComponent: () =>
          import('./nav-1-2-1/nav-1-2-1.component').then((m) => m.Nav121Component),
      },
      {
        path: 'nav-2',
        loadComponent: () =>
          import('./nav-2/nav-2.component').then((m) => m.Nav2Component),
      },
      {
        path: 'nav-2/1',
        loadComponent: () =>
          import('./nav-2-1/nav-2-1.component').then((m) => m.Nav21Component),
      },
      {
        path: 'nav-2/2',
        loadComponent: () =>
          import('./nav-2-2/nav-2-2.component').then((m) => m.Nav22Component),
      },
    ],
  },
];
