import { Component } from '@angular/core';
import { MatxNavTreeComponent, MatxNavTreeItem, MatxSidenavMenuService } from 'matx-core';
import { MatSidenav, MatSidenavContainer, MatSidenavContent } from '@angular/material/sidenav';
import { provideRouter, RouterOutlet } from '@angular/router';
import { MATX_NAV_TREE_DEMO_ROUTES } from './matx-nav-tree-demo.routes';

@Component({
  selector: 'matx-nav-tree-demo',
  templateUrl: './matx-nav-tree-demo.component.html',
  imports: [
    MatSidenavContainer,
    MatxNavTreeComponent,
    RouterOutlet,
    MatSidenav,
    MatSidenavContent,
  ],
  styleUrls: ['./matx-nav-tree-demo.component.scss'],
})
export class MatxNavTreeDemoComponent {
  navItems: MatxNavTreeItem[] = [
    {
      displayName: 'DevFestFL',
      iconName: 'recent_actors',
      route: '/matx-nav-tree',
      children: [
        {
          displayName: 'Speakers',
          iconName: 'group',
          route: '/matx-nav-tree/nav-1',
          children: [
            {
              displayName: 'Michael Prentice',
              iconName: 'person',
              route: '/matx-nav-tree/nav-1/1',
              children: [
                {
                  displayName: 'Create Enterprise UIs',
                  iconName: 'star_rate',
                  route: '/matx-nav-tree/nav-1/1/1',
                },
              ],
            },
            {
              displayName: 'Stephen Fluin',
              iconName: 'person',
              route: '/matx-nav-tree/nav-1/2',
              children: [
                {
                  displayName: "What's up with the Web?",
                  iconName: 'star_rate',
                  route: '/matx-nav-tree/nav-1/2/1',
                },
              ],
            },
          ],
        },
        {
          displayName: 'Sessions',
          iconName: 'speaker_notes',
          route: '/matx-nav-tree/nav-2',
          children: [
            {
              displayName: 'Create Enterprise UIs',
              iconName: 'star_rate',
              route: '/matx-nav-tree/nav-2/1',
            },
            {
              displayName: "What's up with the Web?",
              iconName: 'star_rate',
              route: '/matx-nav-tree/nav-2/2',
            },
          ],
        },
        {
          displayName: 'Google',
          iconName: 'help_outline',
          href: 'https://www.google.com',
        },
      ],
    },
    {
      displayName: 'Disney',
      iconName: 'videocam',
      onItemSelected: (item, index) => {
        window.alert(`You clicked: ${item.displayName}, index: ${index}`);
      },
    },
  ];

  constructor(public sidenavMenuCtrl: MatxSidenavMenuService) {}
}
