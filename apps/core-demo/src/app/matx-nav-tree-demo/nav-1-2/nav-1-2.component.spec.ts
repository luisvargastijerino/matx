import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Nav12Component } from './nav-1-2.component';

describe('Nav12Component', () => {
  let component: Nav12Component;
  let fixture: ComponentFixture<Nav12Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [Nav12Component]
    })
    .compileComponents();

    fixture = TestBed.createComponent(Nav12Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
