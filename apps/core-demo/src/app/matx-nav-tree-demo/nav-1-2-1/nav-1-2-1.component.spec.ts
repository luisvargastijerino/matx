import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Nav121Component } from './nav-1-2-1.component';

describe('Nav121Component', () => {
  let component: Nav121Component;
  let fixture: ComponentFixture<Nav121Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [Nav121Component]
    })
    .compileComponents();

    fixture = TestBed.createComponent(Nav121Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
