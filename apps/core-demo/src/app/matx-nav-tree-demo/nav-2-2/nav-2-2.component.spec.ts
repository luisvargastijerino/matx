import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Nav22Component } from './nav-2-2.component';

describe('Nav22Component', () => {
  let component: Nav22Component;
  let fixture: ComponentFixture<Nav22Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [Nav22Component]
    })
    .compileComponents();

    fixture = TestBed.createComponent(Nav22Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
