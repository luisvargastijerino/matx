import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Nav11Component } from './nav-1-1.component';

describe('Nav11Component', () => {
  let component: Nav11Component;
  let fixture: ComponentFixture<Nav11Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [Nav11Component]
    })
    .compileComponents();

    fixture = TestBed.createComponent(Nav11Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
