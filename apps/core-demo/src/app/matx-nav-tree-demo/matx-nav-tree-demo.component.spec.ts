import { waitForAsync, ComponentFixture, TestBed } from '@angular/core/testing';

import { MatxNavTreeDemoComponent } from './matx-nav-tree-demo.component';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { provideRouter } from '@angular/router';

describe('MatxNavTreeDemoComponent', () => {
  let component: MatxNavTreeDemoComponent;
  let fixture: ComponentFixture<MatxNavTreeDemoComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [MatxNavTreeDemoComponent, NoopAnimationsModule],
      providers: [provideRouter([])],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MatxNavTreeDemoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
