import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Nav21Component } from './nav-2-1.component';

describe('Nav21Component', () => {
  let component: Nav21Component;
  let fixture: ComponentFixture<Nav21Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [Nav21Component]
    })
    .compileComponents();

    fixture = TestBed.createComponent(Nav21Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
