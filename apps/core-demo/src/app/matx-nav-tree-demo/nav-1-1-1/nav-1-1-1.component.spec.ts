import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Nav111Component } from './nav-1-1-1.component';

describe('Nav111Component', () => {
  let component: Nav111Component;
  let fixture: ComponentFixture<Nav111Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [Nav111Component]
    })
    .compileComponents();

    fixture = TestBed.createComponent(Nav111Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
