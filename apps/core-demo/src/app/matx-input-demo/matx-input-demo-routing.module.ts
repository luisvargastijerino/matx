import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MatxInputDemoComponent } from './matx-input-demo.component';

const routes: Routes = [{ path: '', component: MatxInputDemoComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MatxInputDemoRoutingModule {}
