import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MatxInputDemoComponent } from './matx-input-demo.component';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

describe('MatxInputDemoComponent', () => {
  let component: MatxInputDemoComponent;
  let fixture: ComponentFixture<MatxInputDemoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [MatxInputDemoComponent, NoopAnimationsModule],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MatxInputDemoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
