import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatxErrorsComponent, MatxInputComponent } from 'matx-core';
import { MatIcon } from '@angular/material/icon';
import { MatError, MatFormField } from '@angular/material/form-field';
import { JsonPipe } from '@angular/common';
import { ValidatorsModule } from 'ngx-validators';
import { MatInput } from '@angular/material/input';

@Component({
  selector: 'matx-input-demo',
  imports: [
    MatxInputComponent,
    MatIcon,
    FormsModule,
    MatxErrorsComponent,
    MatFormField,
    JsonPipe,
    ReactiveFormsModule,
    ValidatorsModule,
    MatInput,
    MatError,
  ],
  templateUrl: './matx-input-demo.component.html',
})
export class MatxInputDemoComponent implements OnInit {
  form1Model = {
    input1: 'test1',
    matxInput1: 'test1',
    numberInput1: 1,
    matxNumberInput1: 1,
  };

  form2!: FormGroup;

  constructor(private fb: FormBuilder) {}

  ngOnInit(): void {
    this.form2 = this.fb.group({
      input2: 'test2',
      matxInput2: 'test2',
      numberInput2: 1,
      matxNumberInput2: 1,
    });
  }
}
