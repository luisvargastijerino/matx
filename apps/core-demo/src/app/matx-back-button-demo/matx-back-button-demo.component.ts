import { Component } from '@angular/core';
import { MatxBackButtonComponent } from 'matx-core';

@Component({
  selector: 'matx-back-button-demo',
  imports: [MatxBackButtonComponent],
  templateUrl: './matx-back-button-demo.component.html',
})
export class MatxBackButtonDemoComponent {}
