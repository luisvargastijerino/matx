import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MatxBackButtonDemoComponent } from './matx-back-button-demo.component';

const routes: Routes = [{path: '', component: MatxBackButtonDemoComponent}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MatxBackButtonDemoRoutingModule { }
