import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MatxMenuButtonDemoComponent } from './matx-menu-button-demo.component';

const routes: Routes = [{path: '', component: MatxMenuButtonDemoComponent}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MatxMenuButtonDemoRoutingModule { }
