import { Component } from '@angular/core';
import { MatxMenuButtonComponent } from 'matx-core';

@Component({
  selector: 'matx-menu-button-demo',
  imports: [MatxMenuButtonComponent],
  templateUrl: './matx-menu-button-demo.component.html',
})
export class MatxMenuButtonDemoComponent {}
