import { getSidenavToolbar } from '../support/app.po';

describe('core-demo-e2e', () => {
  beforeEach(() => cy.visit('/'));

  it('should display `Matx Core` in SideNavToolbar', () => {
    // Function helper example, see `../support/app.po.ts` file
    getSidenavToolbar().contains(/Matx Core/);
  });
});
